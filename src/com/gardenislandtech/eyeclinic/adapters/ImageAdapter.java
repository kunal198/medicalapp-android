/**
 * 
 */
package com.gardenislandtech.eyeclinic.adapters;

import java.io.File;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gardenislandtech.eyeclinic.FullImageActivity;
import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.fragments.FragmentPrescription;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class ImageAdapter extends BaseAdapter {

	private File[] images;
	private Context context;
	public FragmentPrescription delegate;

	public ImageAdapter(Context context) {
		this.context = context;

		File f = this.context
				.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		this.images = f.listFiles();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return this.images.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View counterView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) parent.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		counterView = inflater.inflate(R.layout.grid_view_image_view, parent,
				false);

		if (counterView != null) {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(this.images[position].getPath(), options);
			options.inSampleSize = ApplicationHelper.calculateInSampleSize(
					options, 100, 100);
			options.inJustDecodeBounds = false;

			Bitmap bmp = BitmapFactory.decodeFile(
					this.images[position].getPath(), options);
			bmp = ApplicationHelper.fixOrientation(bmp);

			ImageView imageView = (ImageView) counterView
					.findViewById(R.id.gridView_imageView);
			imageView.setImageBitmap(bmp);
			imageView.setTag(this.images[position].getPath());
			imageView.setOnClickListener(mOnClickListener);
			imageView.setOnLongClickListener(mOnLongClickListener);
		}

		return counterView;
	}

	private OnClickListener mOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(context, FullImageActivity.class);
			intent.putExtra("image_path", v.getTag().toString());
			context.startActivity(intent);
		}
	};

	private OnLongClickListener mOnLongClickListener = new OnLongClickListener() {

		@Override
		public boolean onLongClick(final View v) {

			CharSequence options[] = new CharSequence[] { "Delete" };

			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Please Select...");
			builder.setItems(options, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case 0:
						File f = new File(v.getTag().toString());
						f.delete();
						delegate.fileDeleted();
						break;
					}
				}
			});
			builder.show();

			return true;
		}
	};

}
