/**
 * 
 */
package com.gardenislandtech.eyeclinic.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.Video;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class VideoAdapter extends BaseAdapter {

	private List<Video> videos;

	/**
	 * 
	 */
	public VideoAdapter(List<Video> list) {
		this.videos = list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return videos.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Video getItem(int position) {
		return videos.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return videos.get(position).get_id();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) parent.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.row_video, parent, false);

		TextView title = (TextView) convertView
				.findViewById(R.id.textView_videoTitle);
		title.setText(videos.get(position).get_title());

		TextView description = (TextView) convertView
				.findViewById(R.id.textView_videoDescription);
		description.setText(videos.get(position).get_description());

		convertView.setTag(videos.get(position).get_url());

		return convertView;
	}

}
