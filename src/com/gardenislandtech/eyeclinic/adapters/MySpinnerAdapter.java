/**
 * 
 */
package com.gardenislandtech.eyeclinic.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gardenislandtech.eyeclinic.Medicine;
import com.gardenislandtech.eyeclinic.R;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class MySpinnerAdapter extends ArrayAdapter<Medicine> {

	private Context context;
	private List<Medicine> medicineList;

	public MySpinnerAdapter(Context context, int textViewResourceId,
			List<Medicine> list) {
		super(context, textViewResourceId);

		this.context = context;
		this.medicineList = list;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return medicineList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Medicine getItem(int position) {
		return medicineList.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return medicineList.get(position).get_id();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int position, View convertView,
	 * ViewGroup parent)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// I created a dynamic TextView here, but you can reference your own
		// custom layout for each spinner item
		TextView label = new TextView(context);
		label.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
		label.setTextColor(Color.BLACK);
		label.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources()
				.getDimension(R.dimen.spinner_size));
		label.setText(medicineList.get(position).get_name());
		label.setLines(1);
		label.setEllipsize(TextUtils.TruncateAt.END);
		return label;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rootView = inflater
				.inflate(R.layout.custom_spinner, parent, false);
		TextView txtHeader = (TextView) rootView.findViewById(R.id.spinnerText);
		txtHeader.setText(medicineList.get(position).get_name());
		txtHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, context
				.getResources().getDimension(R.dimen.spinner_size));
		return rootView;

	}

}
