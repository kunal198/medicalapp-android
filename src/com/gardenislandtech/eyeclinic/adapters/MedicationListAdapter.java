/**
 * 
 */
package com.gardenislandtech.eyeclinic.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.gardenislandtech.eyeclinic.Medication;
import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class MedicationListAdapter extends BaseAdapter {

	private List<Medication> list;

	public MedicationListAdapter(List<Medication> medicationsList) {
		this.list = medicationsList;
	}

	@Override
	public int getCount() {
		return this.list.size();
	}

	@Override
	public Object getItem(int position) {
		return this.list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return this.list.get(position).get_id();
	}

	@Override
	public View getView(int position, View counterView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) parent.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		counterView = inflater.inflate(R.layout.row_medication, parent, false);

		TextView medicineName = (TextView) counterView
				.findViewById(R.id.textView_medicineName);
		medicineName.setText(this.list.get(position).get_name());

		TextView medicineDueAtTime = (TextView) counterView
				.findViewById(R.id.textView_medicineDueAt);
		for (int i = 0; i < list.get(position).get_medication_times().size(); i++) {
			if (list.get(position).get_medication_times().get(i)
					.get_marked_finish() == 0) {
				String time = list.get(position).get_medication_times().get(i)
						.get_medication_time();
				String date = list.get(position).get_medication_times().get(i)
						.get_medication_date();
				SimpleDateFormat sdf = new SimpleDateFormat();
				try {
					sdf.applyPattern(ApplicationHelper.TIME_FORMAT);
					GregorianCalendar gc = new GregorianCalendar();
					gc.setTime(sdf.parse(time));
					Calendar calendar = Calendar.getInstance();
					calendar.set(Calendar.HOUR_OF_DAY,
							gc.get(Calendar.HOUR_OF_DAY));
					calendar.set(Calendar.MINUTE, gc.get(Calendar.MINUTE));

					sdf.applyPattern(ApplicationHelper.DATE_FORMAT);
					gc.setTime(sdf.parse(date));
					calendar.set(Calendar.YEAR, gc.get(Calendar.YEAR));
					calendar.set(Calendar.MONTH, gc.get(Calendar.MONTH));
					calendar.set(Calendar.DAY_OF_MONTH,
							gc.get(Calendar.DAY_OF_MONTH));

					if (calendar.getTimeInMillis() <= System
							.currentTimeMillis()) {
						TextView status = (TextView) counterView
								.findViewById(R.id.textView_medicineStatus);
						status.setVisibility(View.VISIBLE);

						Button takeButton = (Button) counterView
								.findViewById(R.id.button_medicineTake);
						takeButton.setTag(this.list.get(position).get_id());
						takeButton.setEnabled(true);
						Button skipButton = (Button) counterView
								.findViewById(R.id.button_medicineSkip);
						skipButton.setTag(this.list.get(position).get_id());
						skipButton.setEnabled(true);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				medicineDueAtTime.setText(time);
				break;
			}
		}

		TextView medicineLastTakenTime = (TextView) counterView
				.findViewById(R.id.textView_medicineLastTaken);
		if (this.list.get(position).get_last_taken() == null)
			medicineLastTakenTime.setText("Not taken yet.");
		else
			medicineLastTakenTime.setText("Last Taken At: "
					+ this.list.get(position).get_last_taken());

		return counterView;
	}

}
