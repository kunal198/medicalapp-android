package com.gardenislandtech.eyeclinic.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gardenislandtech.eyeclinic.R;

public class NewsAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater;
	private ArrayList<NewsDetail> detail;

	public NewsAdapter(Activity activity, ArrayList<NewsDetail> detail) {
		this.activity = activity;
		this.detail = detail;
	}

	@Override
	public int getCount() {

		return detail.size();
	}

	@Override
	public Object getItem(int position) {

		return detail.get(position);
	}

	@Override
	public long getItemId(int id) {

		return id;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;

		if (inflater == null)
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null) {
			convertView = inflater
					.inflate(R.layout.layout_list_news_item, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.text_name);
			holder.decription = (TextView) convertView
					.findViewById(R.id.text_descrption);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.title.setText(detail.get(position).getNewsTitle());
		holder.decription.setText(detail.get(position).getNewsDescription());

		return convertView;
	}
}

class ViewHolder {
	TextView title, decription;

}