package com.gardenislandtech.eyeclinic.utils;

import java.io.Serializable;

public class NewsDetail implements Serializable {

	public String newsTitle;
	public String newsDescription;

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}

	public String getNewsDescription() {
		return newsDescription;
	}

	public void setNewsDescription(String newsDescription) {
		this.newsDescription = newsDescription;
	}

}
