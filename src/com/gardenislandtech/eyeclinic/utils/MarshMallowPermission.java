package com.gardenislandtech.eyeclinic.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class MarshMallowPermission {
	public static final int CALL_PHONE_STATE =1;
	public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
	public static final int READ_PHONE_STATE_REQUEST_CODE = 3;
	public static final int CAMERA_REQUEST_CODE = 4;
	public static final int RECORD_PERMISSION = 5;
	public static final int READ_XTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 6;
	Activity activity;

	public MarshMallowPermission(Activity activity) {
		this.activity = activity;
	}
	
	

	public boolean checkPhoneCall() {
		int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
		if (result == PackageManager.PERMISSION_GRANTED) {
			return true;
		} else {
			return false;
		}
	}
	public void requestPermissionForPhoneCall() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
			
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE},
					CALL_PHONE_STATE);
		}
	}
	/*public boolean checkPermissionForREADExternalStorage() {
		int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
		if (result == PackageManager.PERMISSION_GRANTED) {
			return true;
		} else {
			return false;
		}
	}
public boolean checkPermissionForLocation(){
	int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
	int result2 = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
	if (result == PackageManager.PERMISSION_GRANTED&& result2 ==PackageManager.PERMISSION_GRANTED) {
		return true;
	} else {
		return false;
	}

}
	public boolean CheckForRecorder() {
		int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO);
		if (result == PackageManager.PERMISSION_GRANTED) {
			return true;
		} else {
			return false;
		}
	}
	public void requestPermissionForRecord() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.RECORD_AUDIO)) {
			Toast.makeText(activity, "Record Permission Required...",
					Toast.LENGTH_LONG).show();
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO},
					RECORD_PERMISSION);
		}
	}
	public void requestPermissionForLocation() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
			Toast.makeText(activity, "External Storage permission needed. Please allow in App Settings for additional functionality.",
					Toast.LENGTH_LONG).show();
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
					EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
		}
	}public void requestPermissionForFINELOCATION() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
			Toast.makeText(activity, "External Storage permission needed. Please allow in App Settings for additional functionality.",
					Toast.LENGTH_LONG).show();
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
					EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
		}
	}
	public void requestPermissionForExternalStorage() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
			Toast.makeText(activity, "External Storage permission needed. Please allow in App Settings for additional functionality.",
					Toast.LENGTH_LONG).show();
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
					EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
		}
	}

	public void requestPermissionForExternalStorage1() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
			Toast.makeText(activity, "External Storage permission needed. Please allow in App Settings for additional functionality.",
					Toast.LENGTH_LONG).show();
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
					EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
		}
	}


	public boolean checkPermissionForReadPhoneState() {
		int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);
		if (result == PackageManager.PERMISSION_GRANTED) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkPermissionCamera() {
		int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
		if (result == PackageManager.PERMISSION_GRANTED) {
			return true;
		} else {
			return false;
		}
	}

	public void requestPermissionForCamera() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
			Toast.makeText(activity, "External Storage permission needed. Please allow in App Settings for additional functionality.",
					Toast.LENGTH_LONG).show();
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA},
					CAMERA_REQUEST_CODE);
		}
	}

	public void requestPermissionForReadPhoneState() {
		if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_PHONE_STATE)) {
			Toast.makeText(activity, "External Storage permission needed. Please allow in App Settings for additional functionality.",
					Toast.LENGTH_LONG).show();
		} else {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE},
					READ_PHONE_STATE_REQUEST_CODE);
		}
	}*/

}
