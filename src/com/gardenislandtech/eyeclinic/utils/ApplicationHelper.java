/**
 * 
 */
package com.gardenislandtech.eyeclinic.utils;

import java.util.Calendar;
import java.util.List;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.gardenislandtech.eyeclinic.Medication;
import com.gardenislandtech.eyeclinic.adapters.MedicationListAdapter;
import com.gardenislandtech.eyeclinic.database.DatabaseAdapter;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class ApplicationHelper {

	// Database Related Constants
	public static DatabaseAdapter database;
	public static final String DATABASE_NAME = "eye_clinic";

	// Tabs Related Constants
	public static int CURRENT_TAB = 0;

	// Application Preferences Constant
	public static SharedPreferences preferences;

	// Preferences keys
	public static final String FIRST_ALARM_ID = "first_alarm_id";
	public static final String SECOND_ALARM_ID = "second_alarm_id";
	public static final String THIRD_ALARM_ID = "third_alarm_id";
	public static final String FIRST_REMINDER = "first_reminder";
	public static final String SECOND_REMINDER = "second_reminder";
	public static final String THIRD_REMINDER = "third_reminder";

	public static final String FIRST_TIME = "first_time";
	public static final String Second_TIME = "second_time";

	public static final String ALARM_DATE = "alarm_date";

	public static final String FirstNotification = "first_noti";

	public static final String IS_APPOINTMENT_MISSED = "is_appointment_missed";
	public static final String NEW_APPOINTMENT_DATE_TIME = "new_appointment_date_time";
	public static final String OLD_APPOINTMENT_DATE_TIME = "old_appointment_date_time";
	public static final String LAST_APPOINTMENT_DATE_TIME = "last_time";

	

	public static final String TAB_ON_NOTIFICATION = "tab_on_notification";
	public static final String NEAPPOINTMENT_TIME = "text";
	public static final String MESSAGE = "message";
	public static final String MESSAGE_NOTI = "message_noti";
	// Date and time formats
	public static final String TIME_FORMAT = "hh:mm a";
	public static final String DATE_FORMAT = "MMM dd, yyyy";
	public static final String DATE_TIME_FORMAT = "MMM dd, yyyy hh:mm a";
	
	public static final String APMESSAGE="ap_message";
	public static final String LAST_APPOINTMENT="last_appointment";

	// Medication data set and adapter
	public static List<Medication> medicationDataSet;
	public static MedicationListAdapter medicationAdapter;

	public static boolean isTodayDate(int selectedYear,
			int selectedMonthOfYear, int selectedDayOfMonth) {
		Calendar cal = Calendar.getInstance();

		if (cal.get(Calendar.YEAR) == selectedYear
				&& cal.get(Calendar.MONTH) == selectedMonthOfYear
				&& cal.get(Calendar.DAY_OF_MONTH) == selectedDayOfMonth)
			return true;
		else
			return false;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth)
				inSampleSize *= 2;
		}

		return inSampleSize;
	}

	public static Bitmap fixOrientation(Bitmap bmp) {
		if (bmp.getWidth() > bmp.getHeight()) {
			Matrix matrix = new Matrix();
			matrix.postRotate(90);
			bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
					bmp.getHeight(), matrix, true);
		}

		return bmp;
	}

}
