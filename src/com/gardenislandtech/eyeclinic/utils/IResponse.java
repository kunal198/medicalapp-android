package com.gardenislandtech.eyeclinic.utils;

public interface IResponse {
	public void onReceiveResponse(String response);
}
