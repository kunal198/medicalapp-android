package com.gardenislandtech.eyeclinic.utils;

public class Constants {
	public static String SENDER_ID = "738822111165";
	public static final String GCM_DEVICE_TOKEN = "device_token";
	public static final String GCM_PREF = "gcm_pref";
	public static final String DEVICE_TOKEN_API = "http://gardenislandtech.com/backend/web/index.php/site/save-device";
	public static final String GET_NEWS_API = "http://gardenislandtech.com/backend/web/index.php/site/news";
	public static int NOTIFICATION_COUNT = 0;
}
