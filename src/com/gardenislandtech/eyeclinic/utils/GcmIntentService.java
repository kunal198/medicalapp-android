package com.gardenislandtech.eyeclinic.utils;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import com.gardenislandtech.eyeclinic.MainActivity;
import com.gardenislandtech.eyeclinic.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService {
	public static final int NOTIFICATION_ID = 1;
	private static final String TAG = "GCM";
	private NotificationManager mNotificationManager;
	private SharedPreferences prefrences;
	private Editor edit;

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		Log.e("push", "receivedd" + extras.toString() + "");
		/*
		 * try { JSONObject jObj=new JSONObject(extras.toString());
		 * Log.e("final",jObj.getString("message"));
		 * 
		 * } catch (JSONException e) { e.printStackTrace(); }
		 */
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
				// This loop represents the service doing some work.
				Log.i(TAG, "Received: " + extras.toString());
				try {
					if (extras != null) {
						// JSONObject jObj=new JSONObject(extras.toString());
						sendNotification(
								extras.getString("message").toString(),
								extras.getString("title"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	@SuppressWarnings("deprecation")
	private void sendNotification(String msg, String title) {
		prefrences = getSharedPreferences("EyeCare", MODE_APPEND);
		edit = prefrences.edit();
		ApplicationHelper.CURRENT_TAB = 5;
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();
		Notification n = new Notification(R.drawable.ic_launcher, "Eye Care",
				System.currentTimeMillis());
		Intent newIntent = new Intent(this, MainActivity.class);
		// newIntent.putExtra("va", value)
		PendingIntent i = PendingIntent.getActivity(this, 0, newIntent, 0);
		int count = prefrences.getInt("notification_count", 1);

		RemoteViews contentView = new RemoteViews(getPackageName(),
				R.layout.custom_notification);
		// contentView.setImageViewResource(R.id.notification_image,
		// R.drawable.notification_image);
		contentView.setTextViewText(R.id.txtTitle, title);
		contentView.setTextViewText(R.id.txtDescription, msg);
		contentView.setTextViewText(R.id.txtNotification, "" + count);

		n.contentView = contentView;
		n.contentIntent = i;

		// n.setLatestEventInfo(getApplicationContext(), title, msg, i);
		// n.number=prefrences.getInt("notification_count", 1);
		n.flags |= Notification.FLAG_AUTO_CANCEL;
		n.flags |= Notification.DEFAULT_SOUND;
		n.flags |= Notification.DEFAULT_VIBRATE;
		++count;
		edit.putInt("notification_count", count).commit();
		mNotificationManager.notify(NOTIFICATION_ID, n);

	}
}