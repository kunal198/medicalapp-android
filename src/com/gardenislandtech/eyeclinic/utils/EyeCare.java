package com.gardenislandtech.eyeclinic.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class EyeCare {

	public static final String DEVICE_TOKEN = "deviceToken";
	private static EyeCare mInstance;
	private SharedPreferences mPref;
	private static String newPref = "new_pref";

	private String firmName;
	private String userName;
	private String password;
	private String started;
	private String deviceToken;

	public void saveDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
		mPref.edit().putString(DEVICE_TOKEN, deviceToken).commit();
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public String getStarted() {
		return started;
	}

	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userName;
	}

	private EyeCare(Context context) {
		mPref = PreferenceManager.getDefaultSharedPreferences(context);
		reloadPrefrence();
	}

	public static SharedPreferences getMpref(Context context) {
		return context.getSharedPreferences(newPref, 0);
	}

	private void reloadPrefrence() {

		deviceToken = mPref.getString(DEVICE_TOKEN, "");
	}

	public static EyeCare getInstance(Context context) {
		return mInstance == null ? (mInstance = new EyeCare(context))
				: mInstance;
	}

	public String getFirmName() {
		return firmName;
	}

	public void clearPref() {
		mPref.edit().clear().commit();
		reloadPrefrence();
	}

}
