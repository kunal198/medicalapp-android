package com.gardenislandtech.eyeclinic;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

public class FullImageActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_image);

		// imgView = (ImageView) findViewById(R.id.imageView_fullImage);
		// ViewTreeObserver vto = imgView.getViewTreeObserver();
		// vto.addOnPreDrawListener(new OnPreDrawListener() {
		//
		// @Override
		// public boolean onPreDraw() {
		// imageWidth = imgView.getMeasuredWidth();
		// imageHeight = imgView.getMeasuredHeight();
		//
		// Log.i("width", "" + imageWidth);
		// Log.i("height", "" + imageHeight);
		//
		// String imagePath = getIntent().getExtras().getString("image_path");
		//
		// BitmapFactory.Options options = new BitmapFactory.Options();
		// options.inJustDecodeBounds = true;
		// BitmapFactory.decodeFile(imagePath, options);
		// options.inSampleSize =
		// ApplicationHelper.calculateInSampleSize(options, imageWidth,
		// imageHeight);
		// options.inJustDecodeBounds = false;
		//
		// Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);
		// imgView.setImageBitmap(bmp);
		// return true;
		// }
		// });
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		ImageView imgView = (ImageView) findViewById(R.id.imageView_fullImage);
		int width = imgView.getWidth();
		int height = imgView.getHeight();

		Log.i("----- ImageView -----", "width: " + width + " height: " + height);

		String imagePath = getIntent().getExtras().getString("image_path");

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imagePath, options);
		options.inSampleSize = ApplicationHelper.calculateInSampleSize(options,
				width, height);
		options.inJustDecodeBounds = false;

		Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);
		Log.i("----- Bitmap Before Fix-----", "width: " + bmp.getWidth()
				+ " height: " + bmp.getHeight());
		bmp = ApplicationHelper.fixOrientation(bmp);
		Log.i("----- Bitmap After Fix-----", "width: " + bmp.getWidth()
				+ " height: " + bmp.getHeight());

		imgView.setImageBitmap(bmp);
	}

}
