package com.gardenislandtech.eyeclinic;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;

import com.gardenislandtech.eyeclinic.utils.Constants;
import com.gardenislandtech.eyeclinic.utils.EyeCare;
import com.gardenislandtech.eyeclinic.utils.JSONParser;
import com.gardenislandtech.eyeclinic.utils.RegisterGcm;

public class SplashActivity extends Activity {
	private String deviceid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		Log.e("device tokenn",
				"data1"
						+ EyeCare.getInstance(SplashActivity.this)
								.getDeviceToken());
		deviceid = Secure.getString(SplashActivity.this.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("device id", deviceid);
		if (EyeCare.getInstance(this).getDeviceToken() == null
				|| EyeCare.getInstance(this).getDeviceToken().equals("")) {
			new RegisterGcm(this) {
				protected void onTokenReceive(String deviceToken) {
					// Log.e("device tokenn", "data" + deviceToken);
					EyeCare.getInstance(SplashActivity.this).saveDeviceToken(
							deviceToken);
					Log.e("device tokenn",
							"data"
									+ EyeCare.getInstance(SplashActivity.this)
											.getDeviceToken());
					new device_token().execute();
				}
			}.execute();
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		Handler mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				Intent intent = new Intent(getApplicationContext(),
						MainActivity.class);
				startActivity(intent);
			}
		}, 2000);
	}

	public class device_token extends AsyncTask<String, String, String> {

		String url = Constants.DEVICE_TOKEN_API;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				List<NameValuePair> obj = new ArrayList<NameValuePair>();
				obj.add(new BasicNameValuePair("type", "Android"));
				obj.add(new BasicNameValuePair("device_id", EyeCare
						.getInstance(SplashActivity.this).getDeviceToken()));

				obj.add(new BasicNameValuePair("phone_id", deviceid));

				// obj.add(new BasicNameValuePair("phone_id", deviceid));

				JSONParser obj1 = new JSONParser();
				JSONObject obj2 = obj1.makeHttpRequest(url, "POST", obj);
				Log.e("responsee", "" + obj2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			Log.e("responsee", "data" + result);

		}
	}
}
