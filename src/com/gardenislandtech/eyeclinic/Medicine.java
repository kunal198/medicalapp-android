/**
 * 
 */
package com.gardenislandtech.eyeclinic;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class Medicine {

	private long _id;
	private String _name;

	public Medicine() {
		// Empty constructor
	}

	/**
	 * @return the _id
	 */
	public long get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}

	/**
	 * @return the _name
	 */
	public String get_name() {
		return _name;
	}

	/**
	 * @param _name
	 *            the _name to set
	 */
	public void set_name(String _name) {
		this._name = _name;
	}

}
