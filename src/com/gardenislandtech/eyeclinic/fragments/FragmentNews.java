/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.utils.Constants;
import com.gardenislandtech.eyeclinic.utils.JSONParser;
import com.gardenislandtech.eyeclinic.utils.NewsAdapter;
import com.gardenislandtech.eyeclinic.utils.NewsDetail;

public class FragmentNews extends Fragment {
	private ListView listView;
	private ProgressDialog pDialog;
	private TextView txtNews, txtNewsDetail;
	private ArrayList<NewsDetail> newsDetail = new ArrayList<NewsDetail>();
	private SharedPreferences prefrences;
	private Editor edit;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.layout_tab6, container, false);
		listView = (ListView) view.findViewById(R.id.lstNews);
		txtNews = (TextView) view.findViewById(R.id.txtNews);
		txtNewsDetail = (TextView) view.findViewById(R.id.txtLatestNews);
		prefrences = getActivity().getSharedPreferences("EyeCare",
				getActivity().MODE_APPEND);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Log.e("click", "click");
				// FragmentManager fm =
				// getActivity().getSupportFragmentManager();
				// fm.popBackStack(null,
				// FragmentManager.POP_BACK_STACK_INCLUSIVE);

				FragmentTransaction ft = getActivity()
						.getSupportFragmentManager().beginTransaction();
				FragmentNewsDetail fragmentNewsDetail = new FragmentNewsDetail();
				Bundle bundle = new Bundle();
				bundle.putSerializable(FragmentNewsDetail.FRAGMENT_DETAIL,
						newsDetail.get(position));
				fragmentNewsDetail.setArguments(bundle);
				ft.addToBackStack(null);

				ft.replace(R.id.tab6, fragmentNewsDetail,
						"fragment_news_detail");
				ft.commit();
				// FragmentManager fm =
				// getActivity().getSupportFragmentManager();
				// fm.popBackStack(null,
				// FragmentManager.POP_BACK_STACK_INCLUSIVE);
				// AddAddress addAddress = new AddAddress();

				// ft.replace(R.id.container, addAddress);
				// ft.addToBackStack(null);
				// ft.commit();
			}
		});
		return view;
	}

	public class all_news extends AsyncTask<String, String, String> {

		String url = Constants.GET_NEWS_API;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				List<NameValuePair> obj = new ArrayList<NameValuePair>();
				obj.add(new BasicNameValuePair("news", "news"));

				JSONParser obj1 = new JSONParser();
				JSONObject obj2 = obj1.makeHttpRequest(url, "POST", obj);
				Log.e("responsee", "sdsd" + obj2);

				newsDetail.clear();
				parseResponse(obj2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			Log.e("responsee", "data" + result);
			if (newsDetail.size() > 0) {
				NewsAdapter adapter = new NewsAdapter(getActivity(), newsDetail);
				listView.setAdapter(adapter);
			} else {
				listView.setVisibility(View.GONE);
				txtNews.setVisibility(View.VISIBLE);
				txtNewsDetail.setVisibility(View.GONE);
			}

		}
	}

	void parseResponse(JSONObject jObj) {
		try {
			JSONArray jArray = jObj.getJSONArray("data");
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jObjDetail = jArray.getJSONObject(i);
				NewsDetail detail = new NewsDetail();
				detail.setNewsTitle(jObjDetail.getString("Title"));
				detail.setNewsDescription(jObjDetail.getString("Content"));
				newsDetail.add(detail);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		edit = prefrences.edit();
		edit.putInt("notification_count", 1).commit();

		new all_news().execute();

	}

}
