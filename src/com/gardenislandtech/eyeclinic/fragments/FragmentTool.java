/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gardenislandtech.eyeclinic.R;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class FragmentTool extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.layout_tab5, container, false);
	}

	public void onGridButtonClick(View v) {

	}

	public void onVideosButtonClick(View v) {

	}

	public void onPrescriptionsButtonClick(View v) {

	}

}
