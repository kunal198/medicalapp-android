/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import java.util.List;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gardenislandtech.eyeclinic.R;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class FragmentLocation extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_location, container, false);
	}

	public void onDirectionsClick(View v) {
		PackageManager packageManager = getActivity().getPackageManager();
		List<PackageInfo> packageInfo = packageManager
				.getInstalledPackages(PackageManager.GET_ACTIVITIES);
		boolean packageFound = false;
		for (PackageInfo pkgInfo : packageInfo) {
			if (pkgInfo.packageName.equals("com.google.android.apps.maps")) {
				packageFound = true;
				break;
			}
		}

		String uri = "https://maps.google.com/maps?q=4439%20Pahee%20Street,%20Lihue,%20HI%2069766,%20United%20States";
		Intent intent = new Intent(android.content.Intent.ACTION_DEFAULT,
				Uri.parse(uri));

		if (packageFound)
			intent.setClassName("com.google.android.apps.maps",
					"com.google.android.maps.MapsActivity");

		startActivity(intent);

	}

	public void onContactClick(View v) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "contact@ecckauai.com" });
		i.putExtra(Intent.EXTRA_SUBJECT, "Contact Eye Care Center Kauai");
		try {
			startActivity(Intent.createChooser(i, "Send via..."));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(getActivity(),
					"There are no email clients installed.", Toast.LENGTH_SHORT)
					.show();
		}
	}

}
