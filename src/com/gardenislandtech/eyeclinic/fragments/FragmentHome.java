/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.utils.MarshMallowPermission;



/**
 * @author MUHAMMAD SAAD
 * 
 */
public class FragmentHome extends Fragment {
MarshMallowPermission mallowPermission;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mallowPermission=new MarshMallowPermission(getActivity());
		return inflater.inflate(R.layout.layout_tab1, container, false);
	}

	/*
	 * public void onLocationClick(View v) {
	 * 
	 * }
	 */public void onCallClick(View v) {
		 if(!mallowPermission.checkPhoneCall())
		 {
			 mallowPermission.requestPermissionForPhoneCall();
		 }
		 else{
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:8082460051"));
				startActivity(intent); 
		 }

	}

	public void onWebsiteClick(View v) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW);
		browserIntent.setData(Uri.parse("http://ecckauai.com/"));
		startActivity(browserIntent);
	}

	public void onWebsiteLink(View v) {
		Intent browserIntents = new Intent(Intent.ACTION_VIEW);
		browserIntents.setData(Uri.parse("https://ecckauai.ema.md/"));
		startActivity(browserIntents);
	}

	public void onDoctorClick(View v) {

	}
	
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
	        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

	        switch (requestCode) {
	           

	            case MarshMallowPermission.CALL_PHONE_STATE:
	          	  Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:8082460051"));
					startActivity(intent); 
	                break;
	            default:
	                break;
	        }
	    }
}