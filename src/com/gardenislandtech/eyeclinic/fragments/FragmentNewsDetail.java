/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.utils.NewsDetail;

public class FragmentNewsDetail extends Fragment {
	public static String FRAGMENT_DETAIL = "fragment_detail";
	private NewsDetail newsDetail;
	private TextView txtTitleDetail, txtDescriptionDetail;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_tab7, container, false);
		txtTitleDetail = (TextView) view.findViewById(R.id.txtTitleDetails);
		txtDescriptionDetail = (TextView) view
				.findViewById(R.id.txtDescriptionDetails);

		if (savedInstanceState == null) {
			if (getArguments() != null)
				newsDetail = (NewsDetail) getArguments().getSerializable(
						FRAGMENT_DETAIL);
		} else if (savedInstanceState.getSerializable(FRAGMENT_DETAIL) != null)
			newsDetail = (NewsDetail) savedInstanceState
					.getSerializable(FRAGMENT_DETAIL);
		Log.e("titleee", newsDetail.getNewsTitle());
		Log.e("descrption", newsDetail.getNewsDescription());
		txtTitleDetail.setText(newsDetail.getNewsTitle());
		txtDescriptionDetail.setText(newsDetail.getNewsDescription());
		return view;
	}
}