/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.adapters.ImageAdapter;

/**
 * @author MUHAMMAD SAAD
 * 
 */

interface IFileDeleted {
	public void fileDeleted();
}

public class FragmentPrescription extends Fragment implements IFileDeleted {

	private Context context;
	private GridView gridView;
	private String imageFileNameWithPath;

	private static int CAMERA_API_REQUEST = 1010;
	private static int GALLERY_INTENT_REQUEST = 2020;

	public FragmentPrescription(Context ctx) {
		this.context = ctx;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_prescription, container,
				false);
		this.gridView = (GridView) view
				.findViewById(R.id.gridView_prescription);
		// :: Needs to implement loader to indicate user that application is
		// processing data in background.
		// new PopulateImages().execute();
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		populateImages();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Bitmap bitmap = null;
		String imageFileName = null;

		try {
			if (requestCode == CAMERA_API_REQUEST
					&& resultCode == Activity.RESULT_OK) {

				ExifInterface exif = new ExifInterface(
						this.imageFileNameWithPath);
				String orientation = exif
						.getAttribute(ExifInterface.TAG_ORIENTATION);
				Log.i("Orientation", orientation);

				if (data == null) {
					populateImages();
					return;
				}

			} else if (requestCode == GALLERY_INTENT_REQUEST
					&& resultCode == Activity.RESULT_OK) {

				if (data == null)
					return;

				InputStream inStream = context.getContentResolver()
						.openInputStream(data.getData());
				bitmap = BitmapFactory.decodeStream(inStream);
				imageFileName = data.getData().getLastPathSegment();

				File storageDir = context
						.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
				File mFile = new File(storageDir, imageFileName);

				// Save created image
				FileOutputStream out = new FileOutputStream(mFile);
				bitmap.compress(CompressFormat.JPEG, 100, out);
				out.flush();
				out.close();
			}

			populateImages();
		} catch (FileNotFoundException e) {
			Log.i("Exception", "FileNotFoundException");
		} catch (NullPointerException e) {
			Log.i("Exception", "NullPointerException");
		} catch (IOException e) {
			Log.i("Exception", "IOException");
		}
	}

	/*
	 * private class PopulateImages extends AsyncTask<Void, Void, Void> {
	 * 
	 * @Override protected void onPreExecute() { super.onPreExecute();
	 * 
	 * progress.show(); }
	 * 
	 * @Override protected Void doInBackground(Void... params) {
	 * populateImages(); return null; }
	 * 
	 * @Override protected void onPostExecute(Void result) {
	 * super.onPostExecute(result);
	 * 
	 * progress.hide(); } }
	 */

	public void addNewPrescription() {
		CharSequence options[] = new CharSequence[] { "Gallery", "Camera" };

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Take picture from...");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					takePictureByIntent();
					break;

				case 1:
					takePictureByCamera();
					break;
				}
			}
		});

		builder.show();
	}

	private void takePictureByIntent() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		startActivityForResult(intent, GALLERY_INTENT_REQUEST);
	}

	private void takePictureByCamera() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {

			// Create the File where the photo should go
			File photoFile = null;
			try {
				this.imageFileNameWithPath = Long.toString(System
						.currentTimeMillis());
				File storageDir = context
						.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
				photoFile = File.createTempFile(this.imageFileNameWithPath,
						".jpg", storageDir);
				this.imageFileNameWithPath = photoFile.getAbsolutePath();
			} catch (IOException ex) {
				// Error occurred while creating the File
				ex.printStackTrace();
			}

			// Continue only if the File was successfully created
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, CAMERA_API_REQUEST);
			}

		} else {
			Toast.makeText(context, "Camera is not working.",
					Toast.LENGTH_SHORT).show();
		}
	}

	private void populateImages() {
		ImageAdapter mImageAdapter = new ImageAdapter(getActivity());
		mImageAdapter.delegate = this;
		this.gridView.setAdapter(mImageAdapter);
	}

	// Callback(s)

	@Override
	public void fileDeleted() {
		this.populateImages();
	}

}
