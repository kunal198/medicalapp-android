/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.adapters.VideoAdapter;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class FragmentVideo extends Fragment implements OnItemClickListener {

	private ListView videoList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_video, container, false);
		VideoAdapter adapter = new VideoAdapter(
				ApplicationHelper.database.getVideos());
		videoList = (ListView) v.findViewById(R.id.ListView_videos);
		videoList.setAdapter(adapter);
		videoList.setOnItemClickListener(this);
		return v;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View child, int position,
			long id) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW);
		browserIntent.setData(Uri.parse(child.getTag().toString()));
		startActivity(browserIntent);
	}

}
