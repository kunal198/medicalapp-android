/**
 * 
 */
package com.gardenislandtech.eyeclinic.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gardenislandtech.eyeclinic.R;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class FragmentAboutDoc extends Fragment {

	private int doctor_id = 0;

	public FragmentAboutDoc(int docID) {
		this.doctor_id = docID;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_about_doc, container,
				false);
		ImageView doctorImageView = (ImageView) view
				.findViewById(R.id.imageView_doctor);
		doctorImageView.setImageResource(getDoctorImage());
		TextView detailsTextView = (TextView) view
				.findViewById(R.id.textView_docDetails);
		detailsTextView.setText(getDoctorDetails());
		return view;
	}

	private int getDoctorImage() {
		int image = 0;
		switch (this.doctor_id) {
		case 1:
			image = R.drawable.photo_tim;
			break;

		case 2:
			image = R.drawable.photo_jean;
			break;

		case 3:
			image = R.drawable.photo_stacey;
			break;

		case 4:
			image = R.drawable.photo_layne;
			break;

		default:
			image = 0;
			break;
		}
		return image;
	}

	private String getDoctorDetails() {
		String details = null;
		switch (this.doctor_id) { // "\"Hello\"" "Hi,\nHow are you?"
		case 1:
			details = "Timothy Lee MD is a board-certified ophthalmologist who has practiced on Kauai since 1987. "
					+ "He first joined Kauai Medical Group, later known as Kauai Medical Clinic, a multi-specialty g"
					+ "roup practice affiliated with Wilcox Memorial Hospital. After twenty years with the clinic, h"
					+ "e entered private practice in 2007, forming the Eye Care Center of Kauai with partner Dr. Lay"
					+ "ne Hashimoto. They were joined by ophthalmologist Dr. Jean Shein in 2009. \n \nDr. Lee received hi"
					+ "s Bachelor of Arts degree from Johns Hopkins University in Baltimore, Maryland. He received h"
					+ "is Medical Degree from Columbia University College of Physicians and Surgeons in New York Cit"
					+ "y. After a year of internship in internal medicine at the Long Beach VA Medical Center, he co"
					+ "mpleted his ophthalmology residency at the University of California at Irvine College of Medi"
					+ "cine. "
					+ "\n \nBorn and raised in Sacramento, California, Dr. Lee began taking piano lessons at age ni"
					+ "ne. He had never performed in public until 2004, when he was invited to participate in Aloha "
					+ "Medical Mission's \"Four Doctors and a Patient \" piano fundraiser at the Neal Blaisdell Concert"
					+ " Hall in Honolulu. Inspired by the movie \"The Pianist\", he played Chopin's Ballade No. 1 in G"
					+ " Minor after learning the piece on his own. He went on to perform in Aloha Medical Mission's "
					+ "next three piano concert fundraisers in 2005, 2007, and 2009. Locally on Kauai in 2007, he ga"
					+ "ve a solo piano program for a sold-out audience at the Mokihana Club's Winter Concert, in ord"
					+ "er to raise funds for music scholarships. In 2009, Dr. Lee organized and performed in Wilcox "
					+ "Hospital Foundation's fundraiser called \"Music and Medicine\" . This event featured several med"
					+ "ical doctors, including Dr. Jean Shein, who played on the piano as well as discussed how thei"
					+ "r musical interests interrelate with medicine. "
					+ "\n \n Dr. Lee and his wife Leticia reside in Poipu, "
					+ "Kauai, with their daughter Bonnie. He loves listening to and playing music of all genres, inc"
					+ "luding classical, Hawaiian, jazz, new age, standards, and bossa nova. He is fascinated by the"
					+ " healing power of music, and uses a variety of musical selections as therapy to calm his pati"
					+ "ents during eye surgery. He is a long-time sponsor for the Kauai Concert Association. He also"
					+ " enjoys caring for the family pets, including a Moluccan cockatoo named Steinway, several Hol"
					+ "land lop rabbits, numerous Japanese koi, and several marine and freshwater aquaria at his hom"
					+ "e and office.";
			break;

		case 2:
			details = "After graduating cum laude from Dartmouth College, Dr. Jean Shein received her medical degr"
					+ "ee from New York University. Her love for Hawaii was realized during her internship year at t"
					+ "he University of Hawaii. Dr. Shein continued her training in ophthalmology at New York Univer"
					+ "sity and at the Manhattan Eye, Ear, and Throat Hospital, where she served as chief resident i"
					+ "n her senior year. She refined her skills during her fellowship at the University of Californ"
					+ "ia, San Diego. "
					+ "\n \n Dr. Shein specializes in the medical and surgical management of cataracts, dia"
					+ "betes, glaucoma, ocular tumors, and macular degeneration. In addition, Dr. Shein is fellowshi"
					+ "p trained in pediatric eye diseases and ocular misalignment. She also offers premium services"
					+ " including eyelid surgery, mulitfocal lens implants, astigmatic lens implants, Botox treatmen"
					+ "ts and Latisse. "
					+ "\n \nIn her spare time, Dr. Shein enjoys spending time with her husband and childr"
					+ "en.";
			break;

		case 3:
			details = "Dr. Stacey Morinaga was born and raised on Oahu and graduated from Aiea High School. She re"
					+ "ceived her bachelor's degree in Biology from Pacific University in Forest Grove, Oregon. She "
					+ "then went on to complete her optometry degree at the New England College of Optometry in Bost"
					+ "on, Massachusetts where she completed externships in pediatrics, contact lenses, geriatrics a"
					+ "nd ocular disease. She also received training at the Portland and Lowell Veterans Hospitals. "
					+ "She is able to fit a wide range of patients requiring specialty contact lenses including thos"
					+ "e with keratoconus and corneal transplants. "
					+ "\n \nIn her spare time, she likes to go hiking, runnin"
					+ "g, paddleboarding and working at the family restaurant Kiibo. She lives in Lihue with her hus"
					+ "band and three miniature dachshunds.";
			break;

		case 4:
			details = "Dr. Layne Hashimoto, born and raised on Kauai, is a graduate of Kauai High School. He atten"
					+ "ded Pacific University in Forest Grove, OR and graduated with a Bachelor's of Science degree "
					+ "in Visual Science in 1998. He then earned his doctorate degree from the Pacific University Co"
					+ "llege of Optometry in 2002. He completed externships including primary care, ocular disease, "
					+ "contact lens, refractive surgery, and vision therapy. After graduation, Dr. Hashimoto worked "
					+ "for the Heritage Vision Center in Beaverton, Oregon until he decided it was time to return ho"
					+ "me to Kauai. He joined Dr. Timothy Lee as a part of the Kauai Medical Clinic in 2003. In 2007"
					+ ", Dr. Lee and Dr. Hashimoto decided to go into private practice, and formed the Eye Care Cent"
					+ "er of Kauai, Inc. "
					+ "\n \nDr. Hashimoto enjoys spending time with his wife Lindsay, and 3 children La"
					+ "uryn, Brooke, and Blake. He loves playing golf, basketball, tennis, and is an avid sports fan"
					+ ". He is passionate about the success of the Eye Care Center of Kauai, and is always excited t"
					+ "o hear ideas on improvements or advancements that would make the patient experience even bett"
					+ "er. He can be reached by email at lhashimoto@ecckauai.com.";
			break;

		default:
			details = "";
			break;
		}
		return details;
	}
}
