package com.gardenislandtech.eyeclinic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.gardenislandtech.eyeclinic.adapters.MedicationListAdapter;
import com.gardenislandtech.eyeclinic.database.DatabaseAdapter;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

public class MainActivity extends TabActivity {

	@SuppressLint("NewApi")
	public static Activity fa;

	String msg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fa = this;

		ApplicationHelper.database = new DatabaseAdapter(this);
		ApplicationHelper.preferences = getSharedPreferences(getPackageName(),
				MODE_PRIVATE);
		// ActionBar actionBar = getActionBar();
		// actionBar.setTitle("vbn");
		// actionBar.
		if (ApplicationHelper.database.checkDatabase()) {
			// Database exist - Application is already installed
		} else {
			// All one time initializations on application installed goes here
			ApplicationHelper.database.initializeDatabse();
			ApplicationHelper.preferences.edit()
					.putInt(ApplicationHelper.FIRST_REMINDER, 24)
					.putInt(ApplicationHelper.SECOND_REMINDER, 2).commit();
		}

		ApplicationHelper.medicationDataSet = ApplicationHelper.database
				.getMedications();
		ApplicationHelper.medicationAdapter = new MedicationListAdapter(
				ApplicationHelper.medicationDataSet);

		int tab = ApplicationHelper.preferences.getInt(
				ApplicationHelper.TAB_ON_NOTIFICATION, -1);
		if (tab != -1) {
			ApplicationHelper.CURRENT_TAB = tab;
			ApplicationHelper.preferences.edit()
					.putInt(ApplicationHelper.TAB_ON_NOTIFICATION, -1).commit();
		}

		try {
			String appointmentSoon = getIntent().getStringExtra(
					"appointmentSoon");
			Log.e("appointmentSoon", "TIME" + appointmentSoon);
		} catch (Exception e) {

		}

		try {
			msg = getIntent().getStringExtra("NotificationMessage");
			Log.e("BalliDaku", "TIME" + msg);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Setting tabs
		setupTabOne();
		setupTabTwo(msg);
		setupTabThree();
		setupTabFour();
		setupTabFive();
		setupTabSix();

		/*
		 * Bundle extras = getIntent().getExtras(); if(extras != null) {
		 * if(extras.containsKey("NotificationMessage")) {
		 * 
		 * // extract the extra-data in the Notification msg =
		 * extras.getString("NotificationMessage");
		 * 
		 * 
		 * Log.e("BalliDaku","TIME" +msg);
		 * 
		 * } else { Log.e("BalliDaku456","TIME" +msg); } } else {
		 * Log.e("BalliDaku123","TIME" +msg); }
		 */

	}

}
