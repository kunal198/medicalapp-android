/**
 * 
 */
package com.gardenislandtech.eyeclinic;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class Video {

	private long _id;
	private String _url;
	private String _title;
	private String _description;

	/**
	 * @return the _id
	 */
	public long get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(long _id) {
		this._id = _id;
	}

	/**
	 * @return the _url
	 */
	public String get_url() {
		return _url;
	}

	/**
	 * @param _url
	 *            the _url to set
	 */
	public void set_url(String _url) {
		this._url = _url;
	}

	/**
	 * @return the _title
	 */
	public String get_title() {
		return _title;
	}

	/**
	 * @param _title
	 *            the _title to set
	 */
	public void set_title(String _title) {
		this._title = _title;
	}

	/**
	 * @return the _description
	 */
	public String get_description() {
		return _description;
	}

	/**
	 * @param _description
	 *            the _description to set
	 */
	public void set_description(String _description) {
		this._description = _description;
	}

}
