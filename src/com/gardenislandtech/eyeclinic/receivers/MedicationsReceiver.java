/**
 * 
 */
package com.gardenislandtech.eyeclinic.receivers;

import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.gardenislandtech.eyeclinic.MainActivity;
import com.gardenislandtech.eyeclinic.Medication;
import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.database.DatabaseAdapter;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class MedicationsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent i = new Intent(context, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pIntent = PendingIntent.getActivity(context,
				(int) System.currentTimeMillis(), i,
				PendingIntent.FLAG_ONE_SHOT);
		DatabaseAdapter adpter = new DatabaseAdapter(context);
		String medicineName = intent.getExtras().getString("medicine_name");

		ApplicationHelper.CURRENT_TAB = 2;
		List<Medication> details = adpter.getMedications();
		for (int k = 0; k < details.size(); k++) {
			Log.e("detailss", "" + details.get(k).get_name());
			if (medicineName.equals(details.get(k).get_name())) {
				Notification notif = new NotificationCompat.Builder(context)
						.setSmallIcon(R.drawable.ic_launcher)
						.setContentIntent(pIntent)
						.setContentTitle("Eye Clinic")
						.setContentText("Time to take '" + medicineName + "'")
						.build();

				notif.defaults = Notification.DEFAULT_ALL;
				notif.flags = Notification.FLAG_AUTO_CANCEL;

				NotificationManager notifManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);
				notifManager.notify((int) System.currentTimeMillis(), notif);

				ApplicationHelper.preferences.edit()
						.putInt(ApplicationHelper.TAB_ON_NOTIFICATION, 2)
						.commit();
			}
		}

	}

}
