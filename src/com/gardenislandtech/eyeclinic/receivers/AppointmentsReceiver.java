/**
 * 
 */
package com.gardenislandtech.eyeclinic.receivers;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

import com.gardenislandtech.eyeclinic.MainActivity;
import com.gardenislandtech.eyeclinic.R;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

/**
 * @author MUHAMMAD SAAD
 * 
 */
public class AppointmentsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent i = new Intent(context, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pIntent = PendingIntent.getActivity(context,
				(int) System.currentTimeMillis(), i,
				PendingIntent.FLAG_ONE_SHOT);

		SharedPreferences preferences = context.getSharedPreferences(
				context.getPackageName(), Context.MODE_PRIVATE);
		ApplicationHelper.CURRENT_TAB = 1;
		Notification notif = null;
		if (preferences.getInt(ApplicationHelper.FIRST_ALARM_ID, 0) != 0) {
			notif = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentIntent(pIntent)
					.setContentTitle("Eye Clinic")
					.setContentText(
							"Appointment with Eye Doctor in "
									+ preferences.getString(
											ApplicationHelper.FIRST_TIME, "")
									+ " hour(s)").build();

			notif.defaults = Notification.DEFAULT_ALL;
			notif.flags = Notification.FLAG_AUTO_CANCEL;
		} else if (preferences.getInt(ApplicationHelper.SECOND_ALARM_ID, 0) != 0) {
			notif = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentIntent(pIntent)
					.setContentTitle("Eye Clinic")
					.setContentText(
							"Appointment with Eye Doctor in "
									+ preferences.getString(
											ApplicationHelper.Second_TIME, "")
									+ " hour(s)").build();

			notif.defaults = Notification.DEFAULT_ALL;
			notif.flags = Notification.FLAG_AUTO_CANCEL;
		} else {
			notif = new NotificationCompat.Builder(context)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentIntent(pIntent).setContentTitle("Eye Clinic")
					.setContentText("Appointment Missed !").build();

			notif.defaults = Notification.DEFAULT_ALL;
			notif.flags = Notification.FLAG_AUTO_CANCEL;
		}

		/*
		 * NotificationManager notifManager = (NotificationManager)
		 * context.getSystemService(Context.NOTIFICATION_SERVICE);
		 * notifManager.notify((int) System.currentTimeMillis(), notif);
		 * 
		 * preferences.edit().putInt(ApplicationHelper.TAB_ON_NOTIFICATION,
		 * 1).commit();
		 * 
		 * Log.e("first notification",""+preferences.getInt(ApplicationHelper.
		 * FIRST_ALARM_ID,0));
		 * Log.e("second notification",""+preferences.getInt(
		 * ApplicationHelper.SECOND_ALARM_ID,0));
		 * 
		 * 
		 * if (preferences.getInt(ApplicationHelper.FIRST_ALARM_ID, 0) != 0) {
		 * preferences.edit().putInt(ApplicationHelper.FIRST_ALARM_ID,
		 * 0).commit(); } else
		 * if(preferences.getInt(ApplicationHelper.SECOND_ALARM_ID, 0) != 0) {
		 * preferences.edit().putInt(ApplicationHelper.SECOND_ALARM_ID,
		 * 0).commit(); } else {
		 * preferences.edit().putInt(ApplicationHelper.THIRD_REMINDER,
		 * 0).commit();
		 * 
		 * } if (preferences.getInt(ApplicationHelper.THIRD_REMINDER,0)==0) {
		 * Log.e("second alarm idd","truee");
		 * preferences.edit().putBoolean("is_appointment_missed",
		 * true).commit(); } }
		 */

	}
}
