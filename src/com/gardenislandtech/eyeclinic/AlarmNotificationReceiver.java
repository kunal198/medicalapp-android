package com.gardenislandtech.eyeclinic;

/**
 * Created by brst-pc17 on 7/1/16.
 */
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class AlarmNotificationReceiver extends BroadcastReceiver {
	String extra;

	@Override
	public void onReceive(Context context, Intent intent) {

		extra = intent.getStringExtra("extra");
		Log.e("extra", "TIME" + extra.toString());
		// Toast.makeText(context,extra,Toast.LENGTH_LONG).show();

		// Notify(context,"Eye Care Kauai",extra);

		generateNotification(context, extra, true);

		/*
		 * if(ApplicationHelper.preferences.getInt(ApplicationHelper.FIRST_ALARM_ID ,1)==1) {
		 * ApplicationHelper.preferences.edit().putInt(ApplicationHelper .FIRST_ALARM_ID, 0).commit(); Log.e("LOG!1",
		 * "TIME");
		 * 
		 * } else if(ApplicationHelper.preferences.getInt(ApplicationHelper.SECOND_ALARM_ID ,2)==2) {
		 * ApplicationHelper.preferences.edit().putInt(ApplicationHelper .SECOND_ALARM_ID, 0).commit();
		 * 
		 * } else { ApplicationHelper.preferences.edit().putInt(ApplicationHelper .THIRD_ALARM_ID, 0).commit();
		 * 
		 * }
		 */

	}

	// 07-05 13:31:00.742: E/extra(28468): TIMEMissed Appointment

	NotificationManager notificationManager;

	public void generateNotification(Context context, String message, boolean sound) {
		int icon = R.drawable.ic_launcher;

		try {

			notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			String title = "Eye Clinic";

			Intent notificationIntent = null;

			notificationIntent = new Intent(context, MainActivity.class);

			notificationIntent.setAction("Notification");

			if (extra.equals("Missed Appointment")) {
				notificationIntent.putExtra("NotificationMessage", "Missed Appointment");
			} else {
				notificationIntent.putExtra("NotificationMessage", "Appointment Soon");
			}

			notificationIntent.addCategory(Intent.CATEGORY_HOME);
			notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);

			PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);

			Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

			if (defaultSound == null) {
				defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
				if (defaultSound == null) {
					defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
				}
			}

			NotificationCompat.Builder builder;

			if (sound) {
				builder = new NotificationCompat.Builder(context).setContentTitle(title).setContentText(message)
						.setContentIntent(intent).setSmallIcon(icon).setLights(Color.MAGENTA, 1, 2)
						.setAutoCancel(true).setSound(defaultSound);
			} else {
				builder = new NotificationCompat.Builder(context).setContentTitle(title).setContentText(message)
						.setContentIntent(intent).setSmallIcon(icon).setLights(Color.MAGENTA, 1, 2)
						.setAutoCancel(true);
			}

			android.app.Notification not = new NotificationCompat.BigTextStyle(builder).bigText(message).build();

			// 100 for all except uploading
			notificationManager.notify(100, not);

		} catch (Exception e) {
			e.printStackTrace();
		} catch (Error e) {
			e.printStackTrace();
		}

	}

}
