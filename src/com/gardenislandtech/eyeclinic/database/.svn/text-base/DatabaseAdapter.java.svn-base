/**
 * 
 */
package com.gardenislandtech.eyeclinic.database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.gardenislandtech.eyeclinic.Medication;
import com.gardenislandtech.eyeclinic.MedicationTime;
import com.gardenislandtech.eyeclinic.Medicine;
import com.gardenislandtech.eyeclinic.Video;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author MUHAMMAD SAAD
 *
 */
public class DatabaseAdapter extends SQLiteOpenHelper {

	private File databaseFile;

	// Database table names
	private final String table_videos = "videos";
	private final String table_Medicines = "Medicines";
	private final String table_Medications = "Medications";
	private final String table_MedicationTimes = "MedicationTimes";

	public DatabaseAdapter(Context context) {
		super(context, ApplicationHelper.DATABASE_NAME, null, 1);
		
		this.databaseFile = context.getDatabasePath(ApplicationHelper.DATABASE_NAME);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	// Custom Methods
    public boolean checkDatabase() {
        return this.databaseFile.exists() ? true : false;
    }
    
    public void initializeDatabse() {
    	String createVideosTable = "CREATE TABLE " + table_videos + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
    			"title TEXT," +
    			"description TEXT," +
    			"url TEXT);";
    	
    	String createMedicinesTable = "CREATE TABLE " + table_Medicines + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
    			"medicine_name TEXT);";
    	
    	String createMedicationsTable = "CREATE TABLE " + table_Medications + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
    			"medicine_id INTEGER," +
    			"start_date TEXT," +
    			"end_date TEXT," +
    			"last_taken_time TEXT," +
    			"FOREIGN KEY(medicine_id) REFERENCES Medicines(_id));";
    	
    	String createMedicationTimesTable = "CREATE TABLE " + table_MedicationTimes + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
    			"medication_id INTEGER," +
    			"medication_time TEXT," +
    			"medication_date TEXT," +
    			"finished INTEGER DEFAULT 0," +
    			"FOREIGN KEY(medication_id) REFERENCES Medications(_id));";
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.execSQL(createVideosTable);
    	db.execSQL(createMedicinesTable);
    	db.execSQL(createMedicationsTable);
    	db.execSQL(createMedicationTimesTable);
    	
    	// Temporary commands ------------------------------------------------------------------------------------
    	String InsertIntoVideosOne = "INSERT INTO " + table_videos + "(title, description, url) VALUES " +
    			"('ACUVUE Brand', 'How to put on and take off contact lenses', 'http://www.youtube.com/watch?v=7gSltoRSFCU');";
    	db.execSQL(InsertIntoVideosOne);
    	
    	String InsertIntoMedicinesOne = "INSERT INTO " + table_Medicines + "(medicine_name) VALUES ('AK-Pentolate');";
    	String InsertIntoMedicinesTwo = "INSERT INTO " + table_Medicines + "(medicine_name) VALUES ('Acetazolamide');";
    	String InsertIntoMedicinesThree = "INSERT INTO " + table_Medicines + "(medicine_name) VALUES ('Acular');";
    	db.execSQL(InsertIntoMedicinesOne);
    	db.execSQL(InsertIntoMedicinesTwo);
    	db.execSQL(InsertIntoMedicinesThree);
    	// ---------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--------------------------------------

    	db.close();
    }
    
    public List<Video> getVideos() {
    	String sql = "SELECT * FROM " + table_videos;
    	
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);
    	
    	List<Video> videos = new ArrayList<Video>();
    	
    	if (c.moveToFirst()) {
    		do {
    			Video v = new Video();
    			v.set_id(c.getLong(0));
    			v.set_title(c.getString(1));
    			v.set_description(c.getString(2));
    			v.set_url(c.getString(3));

    			videos.add(v);
    		} while (c.moveToNext());
    	}
    	
    	db.close();
    	
    	return videos;
    }
    
    public String findMedicineNameById(long id) {
    	String sql = "SELECT medicine_name FROM " + table_Medicines + "WHERE _id = " + id;
    	
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);
    	String medicineName = c.getString(0);
    	db.close();

    	return medicineName;
    }
    
    public long findMedicineIdByName(String name) {
    	String sql = "SELECT _id FROM " + table_Medicines + " WHERE medicine_name = '" + name + "'";
    	
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);
    	long id = 0;

    	if (c.moveToFirst())
    		id = c.getLong(0);
    	db.close();
    	
    	return id;
    }
    
    public List<Medicine> getMedicines() {
//    	String sql = "SELECT m1.* FROM " + table_Medicines + " AS m1, " + table_Medications + " AS m2 " +
//    			"WHERE m1._id != m2.medicine_id";
    	String sql = "SELECT * FROM " + table_Medicines;

    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);

    	List<Medicine> medicineList = new ArrayList<Medicine>();

    	if (c.moveToFirst()) {
    		do {
    			Medicine m = new Medicine();
    			m.set_id(c.getLong(0));
    			m.set_name(c.getString(1));
    			medicineList.add(m);
    		} while (c.moveToNext());
    	}

    	db.close();
    	
    	return medicineList;
    }

    public long insertMedication(Medication m) {
    	ContentValues values = new ContentValues();
    	values.put("medicine_id", this.findMedicineIdByName(m.get_name()));
    	values.put("start_date", m.get_start_date());

    	if (m.get_end_date() != null)
    		values.put("end_date", m.get_end_date());
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	long id = db.insert(table_Medications, null, values);
    	insertMedicationTimes(m.get_medication_times(), id);
    	db.close();
    	return id;
    }
    
    public Medication getMedicationById(long id) {
    	String sql = "SELECT *, (SELECT medicine_name FROM " + table_Medicines + 
    			" WHERE _id = m.medicine_id) AS name FROM " + table_Medications + " AS m WHERE _id = " + id;
    	
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);

    	Medication m = new Medication();

    	if (c.moveToFirst()) {
    		m.set_id(c.getLong(0));
			m.set_start_date(c.getString(2));
			m.set_end_date(c.getString(3));
			m.set_last_taken(c.getString(4));
			m.set_name(c.getString(5));
			m.set_medication_times(this.getMedicationTimesByMedicationId(m.get_id()));
    	}
    	
    	db.close();
    	return m;
    }
    
    public List<Medication> getMedications() {
    	String sql = "SELECT *, (SELECT medicine_name FROM " + table_Medicines + 
    			" WHERE _id = m.medicine_id) AS name FROM " + table_Medications + " AS m;";

    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);

    	List<Medication> medicationList = new ArrayList<Medication>();

    	if (c.moveToFirst()) {
    		do {
    			Medication m = new Medication();
    			m.set_id(c.getLong(0));
    			m.set_start_date(c.getString(2));
    			m.set_end_date(c.getString(3));
    			m.set_last_taken(c.getString(4));
    			m.set_name(c.getString(5));
    			m.set_medication_times(this.getMedicationTimesByMedicationId(m.get_id()));
    			medicationList.add(m);
    		} while (c.moveToNext());
    	}

    	c.close();
//    	db.close();
    	return medicationList;
    }

    public boolean checkIfMedicationIsAlreadyScheduled(String medicineName) {
    	String sql = "SELECT _id FROM " + table_Medications +" WHERE medicine_id IN " +
    			"(SELECT _id FROM " + table_Medicines + " WHERE medicine_name = '" + medicineName + "')";

    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);

    	if (c.moveToFirst())
    		return true;
    	else
    		return false;
    }
    
    private void insertMedicationTimes(List<MedicationTime> list, long medication_id) {
    	SQLiteDatabase db = getWritableDatabase();

    	ContentValues values = new ContentValues();
    	values.put("medication_id", medication_id);
    	
    	for (int i = 0; i < list.size(); i++) {
    		values.put("medication_time", list.get(i).get_medication_time());
    		values.put("medication_date", list.get(i).get_medication_date());
    		db.insert(table_MedicationTimes, null, values);
    	}

    	db.close();
    }
    
    public List<MedicationTime> getMedicationTimesByMedicationId(long id) {
    	String sql = "SELECT * FROM " + table_MedicationTimes + " WHERE medication_id = " + id;
    	
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(sql, null);

    	List<MedicationTime> medicationTimesList = new ArrayList<MedicationTime>();

    	if (c.moveToFirst()) {
    		do {
    			MedicationTime mt = new MedicationTime();
    			mt.set_id(c.getLong(0));
    			mt.set_medication_id(c.getLong(1));
    			mt.set_medication_time(c.getString(2));
    			mt.set_medication_date(c.getString(3));
    			mt.set_marked_finish(c.getInt(4));
    			medicationTimesList.add(mt);
    		} while (c.moveToNext());
    	}

    	c.close();
//    	db.close();
    	return medicationTimesList;
    }
    
    public void updateMedicationTime(long id, String date) {
    	String sql = "UPDATE " + table_MedicationTimes + " SET medication_date = '" + date + "' WHERE medication_id = " + id;

    	SQLiteDatabase db = this.getReadableDatabase();

    	db.execSQL(sql);
    	db.close();
    }
    
    public void updateLastTakenTime(long medication_id, String time, boolean timeShouldReset) {
    	String sql_1 = "UPDATE " + table_Medications + " SET last_taken_time = '" + time + "' WHERE _id = " + medication_id;
    	String sql_2 = "UPDATE " + table_MedicationTimes + " SET finished = 0" + " WHERE medication_id = " + medication_id;
    	String sql_3 = "UPDATE " + table_MedicationTimes + " SET finished = 1" +
    			" WHERE _id IN (SELECT _id FROM " + table_MedicationTimes + " WHERE medication_id = " + medication_id +
    			" AND finished = 0 LIMIT 1)";

    	SQLiteDatabase db = this.getReadableDatabase();

    	// This check will handle skip button functionality
    	if (time != null)
    		db.execSQL(sql_1);

    	// This check will handle time recycle functionality - Time should reset or not
    	if (timeShouldReset)
    		db.execSQL(sql_2);
    	else
    		db.execSQL(sql_3);

    	db.close();
    }
    
    public void deleteMedication(long id) {
    	String sql = "DELETE FROM " + table_Medications + " WHERE _id = " + id;

    	SQLiteDatabase db = this.getReadableDatabase();

    	db.execSQL(sql);
    	db.close();
    }
}
