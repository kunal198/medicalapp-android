package com.gardenislandtech.eyeclinic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.text.Format;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

@SuppressLint("NewApi")
public class AppointmentActivity extends TabActivity {

	Calendar calendar;
	private int firstReminder, finalFirstAlarm;
	private int secondReminder;

	private Calendar selectedDateTime;
	private int status = 0;

	private String promptOnWrongDateSelection = "Date must be larger than today.";
	private String secondAlarm = "", firstAlarmString;
	private Button reminderOneButton, reminderTwoButton;

	// private String secondAlarm = "";
	String selectedDateFromPhone, selectedDateFromPicker,
			selectedMonthFromPicker, pickerTime;
	int phoneMonth, pickerMonth, rightphnmonth;
	boolean allSet = false;
	// int againnewTime=0;
	// int secondRemainderagainnewTime=0;
	int forFirstAlarm;
	DatePicker datePicker;
	int valfordone=0;

	public static AlarmManager mAlarmManager;

	public static PendingIntent mNotificationReceiverPendingIntent;

	Intent mNotificationReceiverIntent;
	long appointmentTime;
	int big,small;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getLayoutInflater().inflate(R.layout.activity_appointment,
				(LinearLayout) findViewById(R.id.tab2));

		mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

		// findViewById(R.id.btn_select_dt).setOnClickListener(this);

		mNotificationReceiverIntent = new Intent(this,
				AlarmNotificationReceiver.class);

		// mNotificationReceiverPendingIntent = PendingIntent.getBroadcast(this,
		// 0, mNotificationReceiverIntent, 0);

		selectedDateTime = Calendar.getInstance();

		firstReminder = ApplicationHelper.preferences.getInt(
				ApplicationHelper.FIRST_REMINDER, 24);
		secondReminder = ApplicationHelper.preferences.getInt(
				ApplicationHelper.SECOND_REMINDER, 2);

		reminderOneButton = (Button) findViewById(R.id.button_reminderOne);
		reminderTwoButton = (Button) findViewById(R.id.button_reminderTwo);

		if (firstReminder == 1) // Show 'Hour' instead of 'Hours' - Avoid
								// Grammatical Mistakes
			reminderOneButton.setText(firstReminder + " Hour Before");
		else
			reminderOneButton.setText(firstReminder + "  Hours Before");

		Button reminderTwoButton = (Button) findViewById(R.id.button_reminderTwo);

		if (secondReminder == 1) // Show 'Hour' instead of 'Hours' - Avoid
									// Grammatical Mistakes
			reminderTwoButton.setText(secondReminder + "Hour Before");
		else
			reminderTwoButton.setText(secondReminder + "  Hours Before");

		setupTabOne();
		setupTabThree();
		setupTabFour();
		setupTabFive();
		setupTabSix();
	}

	// Action Methods
	public void onReminderButtonClick(View v) {
		final int tag = Integer.parseInt((String) v.getTag());

		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);

		NumberPicker np = new NumberPicker(this);
		np.setMinValue(1);
		np.setMaxValue(24);
		np.setWrapSelectorWheel(false);
		np.setValue(tag == 1 ? firstReminder : secondReminder);
		Log.e("pickerfirstReminder", "TIME" + firstReminder);
		Log.e("pickerfirstReminder", "TIME" + secondReminder);
		np.setOnValueChangedListener(new OnValueChangeListener() {

			@Override
			public void onValueChange(NumberPicker picker, int oldVal,
					int newVal) {
				switch (tag) {
				case 1:
					Log.e("picker", "" + picker);

					Log.e("oldVal", "" + oldVal);
					Log.e("newVal", "" + newVal);

					ApplicationHelper.preferences
							.edit()
							.putInt(ApplicationHelper.FIRST_REMINDER,
									firstReminder = newVal).commit();
					Log.e("detucttsss", newVal + "");

					Button reminderOneButton = (Button) findViewById(R.id.button_reminderOne);

					if (firstReminder == 1) // Show 'Hour' instead of 'Hours' -
											// Avoid Grammatical Mistakes
						reminderOneButton.setText(firstReminder
								+ " Hour Before");
					else
						reminderOneButton.setText(firstReminder
								+ "  Hours Before");
					break;

				case 2:
					ApplicationHelper.preferences
							.edit()
							.putInt(ApplicationHelper.SECOND_REMINDER,
									secondReminder = newVal).commit();

					reminderTwoButton = (Button) findViewById(R.id.button_reminderTwo);

					if (secondReminder == 1) // Show 'Hour' instead of 'Hours' -
												// Avoid Grammatical Mistakes
						reminderTwoButton.setText(secondReminder
								+ " Hour Before");
					else
						reminderTwoButton.setText(secondReminder
								+ "  Hours Before");
					break;
				}
			}
		});

		Dialog d = new Dialog(this);
		d.setTitle("Remind me before...");
		d.addContentView(np, params);
		d.show();
	}

	/*
	 * public void onSetAppointmentButtonClick(View v) { status=0; new
	 * DatePickerDialog(this, mOnDateSetListener,
	 * selectedDateTime.get(Calendar.YEAR),
	 * selectedDateTime.get(Calendar.MONTH),
	 * selectedDateTime.get(Calendar.DAY_OF_MONTH)).show(); }
	 */

	public void selectDate(View v) {
		
		final Dialog dai = new Dialog(AppointmentActivity.this);
		dai.setContentView(R.layout.date_selector);
		dai.setTitle("Select Date");

		datePicker = (DatePicker) dai.findViewById(R.id.date_picker);
		Button btn_done = (Button) dai.findViewById(R.id.btn_done);

		btn_done.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				 * SimpleDateFormat formatter = new
				 * SimpleDateFormat("dd/MM/yyyy"); String cdate =
				 * formatter.format(datePicker.getCalendarView().getDate());
				 * 
				 * String date=cdate;
				 */

				selectTime(v);

				dai.dismiss();
			}
		});

		dai.show();
	}

	public void selectTime(View v) {
		valfordone=10;
		final Dialog dai = new Dialog(AppointmentActivity.this);
		dai.setContentView(R.layout.time_selector);

		final TimePicker time_picker = (TimePicker) dai
				.findViewById(R.id.time_picker);
		time_picker.setIs24HourView(false);
		Button done = (Button) dai.findViewById(R.id.done_time_select);

		Log.e("done", "TIME");

		done.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.e("indone", "TIME");
				calendar = Calendar.getInstance();
				calendar.set(datePicker.getYear(), datePicker.getMonth(),
						datePicker.getDayOfMonth(),
						time_picker.getCurrentHour(),
						time_picker.getCurrentMinute(), 0);

				Log.e("Date",
						"TIME" + datePicker.getYear() + "/"
								+ datePicker.getMonth() + "/"
								+ datePicker.getDayOfMonth());
				Log.e("Time", "TIME" + time_picker.getCurrentHour() + ":"
						+ time_picker.getCurrentMinute());
				appointmentTime = calendar.getTimeInMillis();

				Log.e("ApointmentTime", "TIME" + appointmentTime);

				Button button_setAppointment = (Button) findViewById(R.id.button_setAppointment);
				// button_setAppointment.setText(datePicker.getMonth()
				// +":"+datePicker.getDayOfMonth()+":"+
				// datePicker.getYear()+"  "
				// +time_picker.getCurrentHour()+":"+time_picker.getCurrentMinute());
				button_setAppointment.setText(getDate(datePicker.getMonth() + 1
						+ ":" + datePicker.getDayOfMonth() + ":"
						+ datePicker.getYear())
						+ "  "
						+ getTime(time_picker.getCurrentHour(),
								time_picker.getCurrentMinute()));
				/*
				 * setAlaram(appointmentTime,0,"1");
				 * 
				 * setAlaram(appointmentTime,60000,"2");
				 */

				dai.dismiss();
			}
		});
		dai.show();
	}

	private String getTime(int hr, int min) {
		java.sql.Time tme = new java.sql.Time(hr, min, 0);// seconds by default
															// set to zero
		Format formatter;
		formatter = new SimpleDateFormat("hh:mm a");
		return formatter.format(tme);
	}

	public String getDate(String date) {

		String output = "";
		try {
			SimpleDateFormat sdfmt1 = new SimpleDateFormat("MM:dd:yyyy");
			SimpleDateFormat sdfmt2 = new SimpleDateFormat("MMM, dd yyyy");
			java.util.Date dDate = sdfmt1.parse(date);
			output = sdfmt2.format(dDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	int i = 0;
	static PendingIntent pi1;
	static PendingIntent pi2;
	static PendingIntent pi3;

	public void setAlaram(long time, long reminder) {

		
		
		if(secondReminder> firstReminder){
			big= secondReminder;
			small=firstReminder;
		}
		else if(secondReminder< firstReminder){
		big=firstReminder;
		small=secondReminder;
		
		}
		Log.e("INSETALARM", "TIME");
		if (i == 0) {
			mNotificationReceiverIntent.putExtra("extra", "Missed Appointment");
			ApplicationHelper.preferences.edit()
					.putInt(ApplicationHelper.THIRD_ALARM_ID, 0).commit();

			pi1 = PendingIntent.getBroadcast(AppointmentActivity.this, i,
					mNotificationReceiverIntent, 0);
			mNotificationReceiverPendingIntent = pi1;

		} else if (i == 1) {
			/*int com;
			if(secondReminder>firstReminder){
			   com=secondReminder;
			}else {
				com=firstReminder;
			}*/
		//	Log.e("comAlarm","TIME"+com);
			
			mNotificationReceiverIntent.putExtra("extra",
					"Appointment with Eye Doctor ins " + firstReminder +" "+"hour(s)");
			ApplicationHelper.preferences.edit()
					.putInt(ApplicationHelper.FIRST_ALARM_ID, 0).commit();

			pi2 = PendingIntent.getBroadcast(AppointmentActivity.this, i,
					mNotificationReceiverIntent, 0);
			mNotificationReceiverPendingIntent = pi2;

		} else if (i == 2) {
			
			/*int comn;
			if(secondReminder<firstReminder){
				comn=secondReminder;
			}
			else {
				comn=firstReminder;
			}*/
			//Log.e("comnAlarm","TIME"+comn);
			mNotificationReceiverIntent.putExtra("extra",
					"Appointment with Eye Doctor in " + secondReminder+" " +"hour(s)");
			ApplicationHelper.preferences.edit()
					.putInt(ApplicationHelper.SECOND_ALARM_ID, 0).commit();

			pi3 = PendingIntent.getBroadcast(AppointmentActivity.this, i,
					mNotificationReceiverIntent, 0);
			mNotificationReceiverPendingIntent = pi3;
Log.e("SMAll","TIME"+small);
Log.e("BIG","TIME"+big);
		}

		mAlarmManager.set(AlarmManager.RTC_WAKEUP, time - reminder,
				mNotificationReceiverPendingIntent);

		i++;

	}

	public static void apointmentCAncel() {

		if (mAlarmManager != null) {
			mAlarmManager.cancel(pi1);
			mAlarmManager.cancel(pi2); 
			mAlarmManager.cancel(pi3);
			Log.e("Helloballid", "TIME");
		} else
			Log.e("HelloballiT", "TIME");

		// Cancel all alarms using mLoggerReceiverPendingIntent
		// mAlarmManager.cancel(mLoggerReceiverPendingIntent);

	}

	public void onSaveAppointmentClick(View v) {
		
		if(valfordone==10){
			Log.e("data",secondAlarm);
			
		boolean abc=	ApplicationHelper.preferences.edit().putString(ApplicationHelper.LAST_APPOINTMENT_DATE_TIME, secondAlarm).commit();	
		Log.e("abc", "TIME"+abc);
		// onSetAppointmentButtonClick(v);
/*
		Log.e("selectedd", "TIME" + selectedDateTime.getTimeInMillis());
		Log.e("selecteddtime", "TIME" + selectedDateTime.getTime());
		Log.e("selecteddzone", "TIME" + selectedDateTime.getTimeZone());

		Log.e("DayMONTH", "TIME" + selectedDateTime.get(Calendar.DAY_OF_MONTH));

		Log.e("YEAR", "TIME" + selectedDateTime.get(Calendar.YEAR));
		Log.e("MONTH", "TIME" + selectedDateTime.get(Calendar.MONTH));*/

		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();

		Date d = new Date();
		String s = (String) DateFormat.format("MMMM d, yyyy ", d.getTime());
		String[] separate = s.split(" ");
		selectedDateFromPhone = separate[1].replace(",", "");
		Log.e("currentDATE", "TIME" + d);
		Log.e("currentDATE", "TIME" + selectedDateFromPhone);

		// 146746500059007-02 16:44:43.292: E/ApointmentTime(12102):
		// TIME1467465360292

		Button btnselectedTime = (Button) findViewById(R.id.button_setAppointment);

		Button reminderOne = (Button) findViewById(R.id.button_reminderOne);
	//	ApplicationHelper.preferences.edit().putString(ApplicationHelper.LAST_APPOINTMENT, null).commit();
		String seletcedDateTime = btnselectedTime.getText().toString();
		ApplicationHelper.preferences.edit().putString(ApplicationHelper.LAST_APPOINTMENT, seletcedDateTime).commit();
		
		String appy=ApplicationHelper.preferences.getString(ApplicationHelper.LAST_APPOINTMENT, "");
		
		Log.e("lastAP", "TIME"+ApplicationHelper.preferences.getString(ApplicationHelper.LAST_APPOINTMENT, ""));
		
		Log.e("lastAP", "TIME"+appy);
		Log.e("seletcedDateTime", "TIME" + seletcedDateTime);

		String[] separated = seletcedDateTime.split(" ");
		Log.e("separatedL","TIME"+separated);
		// separated[0];
		// separated[1];
		//07-05 17:33:21.948: E/finalssssssssss(5851): TIMEJul, 05 2016  07:34 PM

		//Log.e("finalssssssssss", "TIME" + seletcedDateTime);
		ApplicationHelper.preferences
				.edit()
				.putString(ApplicationHelper.NEAPPOINTMENT_TIME,
						seletcedDateTime).commit();

		String month = separated[0].trim();

		String date = separated[1].trim();
		String extra = separated[2].trim();
		String hourmin = separated[4].trim();
		String ampm = separated[5].trim();

		String[] separatedTwo = seletcedDateTime.split(" ");
		String totalTime = separatedTwo[2].trim();
		

		/*
		 * String month = separated[2].trim();
		 * 
		 * String date = separated[1].trim(); String extra =
		 * separated[2].trim(); String hourmin=separated[3].trim();
		 * 
		 * String[] separatedTwo = seletcedDateTime.split(" "); String
		 * totalTime=separatedTwo[2].trim();
		 */

		pickerTime = hourmin.replace(":", "");
	
		
		Log.e("date", "TIME" + date);
		Log.e("separatedTwo", "TIME" + seletcedDateTime.split(" "));
		Log.e("ampm", "TIME" + ampm);
		Log.e("totalTime", "TIME" + hourmin);
		Log.e("pickerTime", "TIME" + pickerTime);
		selectedDateFromPicker = separated[1].replace(",", "");

		// /selectedMonthFromPicker=separated[0].replace(",","");
		selectedMonthFromPicker = month;
		//Log.e("selectedMonthFromPicker", "TIME" + selectedMonthFromPicker);

		// pickerMonth=selectedMonthFromPicker;

		if (selectedMonthFromPicker.equalsIgnoreCase("Jan")) {
			pickerMonth = 1;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Feb")) {
			pickerMonth = 2;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Mar")) {
			pickerMonth = 3;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Apr")) {
			pickerMonth = 4;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("May")) {
			pickerMonth = 5;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Jun")) {
			pickerMonth = 6;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Jul")) {
			pickerMonth = 7;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Aug")) {
			pickerMonth = 8;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Sep")) {
			pickerMonth = 9;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Oct")) {
			pickerMonth = 10;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Nov")) {
			pickerMonth = 11;
		} else if (selectedMonthFromPicker.equalsIgnoreCase("Dec")) {
			pickerMonth = 12;
		}

		separated[3] = separated[3].trim();
		// separated[4] = separated[4].trim();
		// String timeInFormat=separated[4];
	//	Log.e("seletcedDateTime", "TIME" + seletcedDateTime);
	//	Log.e("monthTime", "TIME" + separated[0].trim());
		// Log.e("timeInFormat", "TIME"+timeInFormat);

		long selectedTime = selectedDateTime.getTimeInMillis();
		long currentTime = System.currentTimeMillis();
		long differenceOfTime = selectedTime - currentTime;
		long hours = differenceOfTime / 1000 / 60 / 60;

		// String longV = "1343805819061";

		String selectedTimeFromCal = separated[4];

		String newString = selectedTimeFromCal.replace(":", "");
		/*Log.e("newString", "TIME" + newString);

		Log.e("selectedTimCal", "TIME" + separated[2]);
		Log.e("selectedTimCal3", "TIME" + separated[3]);
		Log.e("selectedTimCal4", "TIME" + separated[4]);
*/
		// String newString = selectedTimeFromCal.replace(":", "");

		int cuTime = Integer.parseInt(newString);
		Log.e("intTime", "TIME" + Integer.parseInt(newString));
		Log.e("intTimecuTime", "TIME" + cuTime);
		// Integer.parseInt(selectedTimeFromCal);

		Log.e("ampmtime", "TIME" + separated[3].trim());
		// Log.e("ampm","TIME"+separated[4].trim());
		String currentTimeFromCal = today.format("%k:%M");

		Log.e("newselectedTime", "TIME" + selectedTime);
		Log.e("newcurrentTimeTime", "TIME" + currentTime);

		Time newToday = new Time();
		newToday.set(selectedTime);
		newToday.format("%k:%M %d.%m.%y");

		// 06-28 15:01:56.238: E/newcurrentTime(28854):
		// TIME20160628T160300Asia/Calcutta(2,179,19800,0,1467109980)

		int oldtime=0;
		int newTime = 0;
		Log.e("newcurrentTime", "TIME" + newToday);
		Log.e("cuTime", "TIME" + cuTime);
		/*
		 * if(timeInFormat.equalsIgnoreCase("PM")){
		 * 
		 * newTime=cuTime+1200;
		 * 
		 * 
		 * } else if(timeInFormat.equalsIgnoreCase("AM")) {
		 * Log.e("cuTimeIN","TIME"+cuTime); newTime=cuTime; }
		 */
		// newTime=cuTime;
		oldtime = Integer.parseInt(pickerTime);
		Log.e("newTime", "TIME" + newTime);
		
if(ampm.equalsIgnoreCase("pm")){
			
	newTime=oldtime+1200;
		}
else{
	newTime=oldtime;
}
		String newselectedTimeFromCal = String.valueOf(newTime);

		Log.e("afterNewTime", "TIME" + newselectedTimeFromCal);

		String newcurrentTimeFromCal = currentTimeFromCal.replace(":", "");

		int addTime = Integer.parseInt(newcurrentTimeFromCal.toString().trim());
		Log.e("selTimeCal", "TIME" + newselectedTimeFromCal);
		Log.e("crtTimeCal", "TIME" + newcurrentTimeFromCal); // sys
																// //newcurrentTimeFromCal

		Log.e("firstRemender", "TIME" + firstReminder);
		Log.e("newTime1", "TIME" + newTime);
		int againnewTime = 0;
		int secondRemainderagainnewTime = 0;
		int phoneDate, pickerDate;
		phoneDate = Integer.parseInt(selectedDateFromPhone);
		pickerDate = Integer.parseInt(selectedDateFromPicker);

		Log.e("phoneDate", "TIME" + phoneDate);
		Log.e("pickerDate", "TIME" + pickerDate);

		Log.e("pickerMonth", "TIME" + pickerMonth);

		Log.e("addTime", "TIME" + addTime);

		// rightphnmonth=phoneMonth+1;
		Log.e("phoneMonth", "TIME" + phoneMonth);
		Log.e("rightphnmonth", "TIME" + rightphnmonth);

		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf1 = new SimpleDateFormat("M");
		String strDate = sdf1.format(c.getTime());

		Log.e("newPhoneMonth", "TIME" + strDate);

		rightphnmonth = Integer.parseInt(strDate);

		/*if (pickerMonth > rightphnmonth) {
			Log.e("doneWell", "TIME");
		}*/

		 if (pickerDate > phoneDate) {
			Log.e("doneWEll", "TIME");
		} else {
			Log.e("addTimeelse", "TIME" + addTime);

			againnewTime = addTime;
			secondRemainderagainnewTime = addTime;
			if (firstReminder == 1) {
				finalFirstAlarm = 100;
				againnewTime = addTime + finalFirstAlarm;

			}

			else if (firstReminder == 2) {
				finalFirstAlarm = 200;
				againnewTime = addTime + finalFirstAlarm;
				Log.e("inTwo", "TIME" + againnewTime);

			} else if (firstReminder == 3) {
				finalFirstAlarm = 300;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 4) {
				finalFirstAlarm = 400;
				againnewTime = addTime + finalFirstAlarm;
				Log.e("again4", "TIME" + againnewTime);

			} else if (firstReminder == 5) {
				finalFirstAlarm = 500;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 6) {
				finalFirstAlarm = 600;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 7) {
				finalFirstAlarm = 700;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 8) {
				finalFirstAlarm = 800;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 9) {
				finalFirstAlarm = 900;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 10) {
				finalFirstAlarm = 1000;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 11) {
				finalFirstAlarm = 1100;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 12) {
				finalFirstAlarm = 1200;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 13) {
				finalFirstAlarm = 1300;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 14) {
				finalFirstAlarm = 1400;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 15) {
				finalFirstAlarm = 1500;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 16) {
				finalFirstAlarm = 1600;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 17) {
				finalFirstAlarm = 1700;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 18) {
				finalFirstAlarm = 1800;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 19) {
				finalFirstAlarm = 1900;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 20) {
				finalFirstAlarm = 2000;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 21) {
				finalFirstAlarm = 2100;
				againnewTime = addTime + finalFirstAlarm;

			}

			else if (firstReminder == 22) {
				finalFirstAlarm = 2200;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 23) {
				finalFirstAlarm = 2300;
				againnewTime = addTime + finalFirstAlarm;

			} else if (firstReminder == 24) {
				finalFirstAlarm = 2400;
				againnewTime = addTime + finalFirstAlarm;

			}

			if (secondReminder == 1) {
				secondRemainderagainnewTime = addTime + 100;

			}

			else if (secondReminder == 2) {
				secondRemainderagainnewTime = addTime + 200;

			} else if (secondReminder == 3) {
				secondRemainderagainnewTime = addTime + 300;

			} else if (secondReminder == 4) {
				secondRemainderagainnewTime = addTime + 400;
				Log.e("again4", "TIME" + againnewTime);

			} else if (secondReminder == 5) {
				secondRemainderagainnewTime = addTime + 500;

			} else if (secondReminder == 6) {
				secondRemainderagainnewTime = addTime + 600;

			} else if (secondReminder == 7) {
				secondRemainderagainnewTime = addTime + 700;

			} else if (secondReminder == 8) {
				secondRemainderagainnewTime = addTime + 800;

			} else if (secondReminder == 9) {
				secondRemainderagainnewTime = addTime + 900;

			} else if (secondReminder == 10) {
				secondRemainderagainnewTime = addTime + 1000;

			} else if (secondReminder == 11) {
				secondRemainderagainnewTime = addTime + 1100;

			} else if (secondReminder == 12) {
				secondRemainderagainnewTime = addTime + 1200;

			} else if (secondReminder == 13) {
				secondRemainderagainnewTime = addTime + 1300;

			} else if (secondReminder == 14) {
				secondRemainderagainnewTime = addTime + 1400;

			} else if (secondReminder == 15) {
				secondRemainderagainnewTime = addTime + 1500;

			} else if (secondReminder == 16) {
				secondRemainderagainnewTime = addTime + 1600;

			} else if (secondReminder == 17) {
				secondRemainderagainnewTime = addTime + 1700;

			} else if (secondReminder == 18) {
				secondRemainderagainnewTime = addTime + 1800;

			} else if (secondReminder == 19) {
				secondRemainderagainnewTime = addTime + 1900;

			} else if (secondReminder == 20) {
				secondRemainderagainnewTime = addTime + 2000;

			} else if (secondReminder == 21) {
				secondRemainderagainnewTime = addTime + 200;

			} else if (secondReminder == 2) {
				secondRemainderagainnewTime = addTime + 2100;

			} else if (secondReminder == 22) {
				secondRemainderagainnewTime = addTime + 2200;

			} else if (secondReminder == 23) {
				secondRemainderagainnewTime = addTime + 2300;

			} else if (secondReminder == 24) {
				secondRemainderagainnewTime = addTime + 2400;

			}
		}

		Log.e("againnewTime", "TIME" + againnewTime);
		String dateString = DateFormat.format("MM/dd/yyyy",
				new Date(currentTime)).toString();
		Log.e("dateInSTRING", "TIME" + dateString);

		String finalTime = String.valueOf(againnewTime);
		// 06-27 19:50:05.877: E/selected(23765): TIME1467040860764
		// 06-27 19:50:05.877: E/system(23765): TIME1467037205877
		if (selectedTime > currentTime) {

			Toast.makeText(this, "Cannot set Please select correct Time ",
					Toast.LENGTH_LONG).show();

			return;
		}

		// if()

		Log.e("newTimeres", "TIME" + newTime);
		Log.e("firstReminderres", "TIME" + firstReminder);

		forFirstAlarm = newTime - finalFirstAlarm;

		Log.e("fagainnewTime", "TIME" + againnewTime);
		Log.e("seconREmainderTime", "TIME" + secondRemainderagainnewTime);

		Log.e("fnewTime", "TIME" + newTime);
		Log.e("forFirstAlarmm", "TIME" + forFirstAlarm);
		allSet = false;

		if (againnewTime > newTime) {

			Toast.makeText(getApplicationContext(),
					"Please enter corret value", 4000).show();
			allSet = true;
		} else if (secondRemainderagainnewTime > newTime) {
			Toast.makeText(getApplicationContext(),
					"Please enter corret value", 4000).show();
			allSet = true;
		}
		/*
		 * if(secondRemainderagainnewTime > newTime){
		 * Toast.makeText(getApplicationContext(), "Please enter corret value",
		 * 4000).show(); }
		 */

		else {
			if (allSet == false) {

				/*
				 * if (firstReminder >= secondReminder) { if (hours >=
				 * firstReminder) scheduleAlarm(secondReminder, firstReminder);
				 * 
				 * 
				 * else if (hours >= secondReminder)
				 * scheduleAlarm(secondReminder, -1);
				 * 
				 * else { SimpleDateFormat sdf = new
				 * SimpleDateFormat(ApplicationHelper.TIME_FORMAT);
				 * Toast.makeText(this, "Cannot set " + secondReminder +
				 * " hours reminder for appointment at " + sdf.format(new
				 * Date(selectedTime)), Toast.LENGTH_LONG).show(); return;
				 * 
				 * 
				 * }
				 * 
				 * }
				 * 
				 * 
				 * 
				 * else if (firstReminder < secondReminder) { if (hours >=
				 * secondReminder) scheduleAlarm(firstReminder, secondReminder);
				 * else if (hours >= firstReminder) scheduleAlarm(firstReminder,
				 * -1); else { SimpleDateFormat sdf = new
				 * SimpleDateFormat(ApplicationHelper.TIME_FORMAT);
				 * Toast.makeText(this, "Cannot set " + firstReminder +
				 * " hours reminder for appointment at " + sdf.format(new
				 * Date(selectedTime)), Toast.LENGTH_LONG).show(); return; } }
				 * 
				 * 
				 * 
				 * else { scheduleAlarm(firstReminder, -1); }
				 * 
				 * setResult(RESULT_OK); finish();
				 */

				// 07-02 19:14:06.150: E/appointmentTimeVl(14832):
				// TIME1467474360304

				long time = System.currentTimeMillis();
				TimeUnit.MINUTES.toMillis(forFirstAlarm);
				/*
				 * Long myLong=; Long myLong1=myLong.getTime();
				 */
				// Log.e("FrtReminMills",
				// "TIME"+TimeUnit.SECONDS.toMillis(forFirstAlarm));
				// Log.e("CorReminMills",
				// "TIME"+TimeUnit.MILLISECONDS.convert(forFirstAlarm,
				// TimeUnit.HOURS));
				//
				// Log.e("CutimeMills", "TIME"+time);
				//
				// Log.e("appointmentTimeVl", "TIME"+appointmentTime);

				ApplicationHelper.preferences.edit()
						.putInt(ApplicationHelper.FIRST_ALARM_ID, 1).commit();
				ApplicationHelper.preferences.edit()
						.putInt(ApplicationHelper.SECOND_ALARM_ID, 2).commit();
				ApplicationHelper.preferences.edit()
						.putInt(ApplicationHelper.THIRD_ALARM_ID, 3).commit();
				setAlaram(appointmentTime, 0);
				setAlaram(appointmentTime, TimeUnit.MILLISECONDS.convert(
						firstReminder, TimeUnit.HOURS));
				setAlaram(appointmentTime, TimeUnit.MILLISECONDS.convert(
						secondReminder, TimeUnit.HOURS));
Log.e("firstReminderIn","TIME"+firstReminder);
				Intent goback = new Intent(AppointmentActivity.this,
						MainActivity.class);
				startActivity(goback);
			}
		}
	}
		else{
			Toast.makeText(AppointmentActivity.this, "Please select Appointment Timing", 2000).show();
}

}

	// Custom Private Methods

	/*private void scheduleAlarm(int current, int next) {
		// Updating preferences
		SimpleDateFormat sdf = new SimpleDateFormat(
				ApplicationHelper.DATE_TIME_FORMAT);
		String newAppointmentDateTime = sdf.format(selectedDateTime.getTime());
		ApplicationHelper.preferences
				.edit()
				.putString(ApplicationHelper.NEW_APPOINTMENT_DATE_TIME,
						newAppointmentDateTime).commit();

		// Schedule first notification
		selectedDateTime.add(Calendar.HOUR_OF_DAY, -current);

		Intent intent = new Intent();
		intent.setAction("com.gardenislandtech.eyeclinic.receivers.AppointmentsReceiver");

		int firstAlarmID = (int) System.currentTimeMillis();
		Long timeInMilliseconds3 = null, timeInMilliseconds5 = null;
		Long detail = selectedDateTime.getTimeInMillis();
		String finalAlarm[] = secondAlarm.split(" ");

		Log.e("firsssttt alarmmmm final", firstAlarmString);

		// First button notificationssssssss

		String reminderOne[] = reminderOneButton.getText().toString()
				.split(" ");
		String reminderOneSTRING = reminderOne[0] + ":00";
		// intent.putExtra("meeting_after", reminderOne);

		// intent.putExtra("meeting_after", reminderOne[0]);

		String alarm1 = "";
		Log.e("set beforree", reminderOneSTRING);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
		try {
			Date date1 = simpleDateFormat.parse(firstAlarmString);
			Date date2 = simpleDateFormat.parse(reminderOneSTRING);

			long difference = date2.getTime() - date1.getTime();
			int days = (int) (difference / (1000 * 60 * 60 * 24));
			int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
			int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours))
					/ (1000 * 60);
			hours = (hours < 0 ? -hours : hours);
			min = (min < 0 ? -min : min);
			Log.e("Hours", " :: " + hours + ":" + min);
			alarm1 = hours + ":" + min;
		} catch (Exception e) {
			e.printStackTrace();
		}

		String alarm = "Jun 30, 2016 " + alarm1 + " PM";
		Log.e("gettingddd", alarm);

		alarm = finalAlarm[0] + " " + finalAlarm[1] + " " + finalAlarm[2] + " "
				+ alarm1 + " " + finalAlarm[4];
		Log.e("gettinggg idss finallsss", alarm);
		SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
		Date mDate1 = null;
		try {
			Log.e("secondssssssssalarmm", secondAlarm);

			mDate1 = sdf2.parse(alarm);
			timeInMilliseconds5 = mDate1.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		int SecondAlarmID = (int) System.currentTimeMillis();

		PendingIntent pIntent = PendingIntent.getBroadcast(this, SecondAlarmID,
				intent, PendingIntent.FLAG_ONE_SHOT);

		AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, timeInMilliseconds5, pIntent);
		// am.set(AlarmManager.RTC_WAKEUP, selectedDateTime.getTimeInMillis(),
		// pIntent);
		ApplicationHelper.preferences.edit()
				.putInt(ApplicationHelper.SECOND_ALARM_ID, SecondAlarmID)
				.commit();
		Log.e("selected time", selectedDateTime.getTimeInMillis() + "");
		Log.e("current", "" + current);
		Log.e("nextt", "" + next);

		// second alarm

		String reminderOne2[] = reminderTwoButton.getText().toString()
				.split(" ");
		String reminderOneSTRING2 = reminderOne2[0] + ":00";

		// intent.putExtra("meeting_after", reminderOne2[0]);

		String alarm2 = "";
		Log.e("set beforree", reminderOneSTRING2);
		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
		try {
			Date date1 = simpleDateFormat1.parse(firstAlarmString);
			Date date2 = simpleDateFormat1.parse(reminderOneSTRING2);

			long difference = date2.getTime() - date1.getTime();
			int days = (int) (difference / (1000 * 60 * 60 * 24));
			int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
			int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours))
					/ (1000 * 60);
			hours = (hours < 0 ? -hours : hours);
			min = (min < 0 ? -min : min);
			Log.e("Hours", " :: " + hours + ":" + min);
			alarm2 = hours + ":" + min;
		} catch (Exception e) {
			e.printStackTrace();
		}

		String alarm5 = "Jun 30, 2016 " + alarm1 + " PM";
		Log.e("gettingddd", alarm);
		alarm2 = finalAlarm[0] + " " + finalAlarm[1] + " " + finalAlarm[2]
				+ " " + alarm2 + " " + finalAlarm[4];
		Log.e("gettinggg idss finallsss", alarm);
		SimpleDateFormat sdf6 = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
		Date mDate6 = null;
		try {
			Log.e("secondssssssssalarmm", secondAlarm);

			mDate6 = sdf6.parse(alarm2);
			timeInMilliseconds5 = mDate6.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PendingIntent pIntentFinal = PendingIntent.getBroadcast(this,
				firstAlarmID, intent, PendingIntent.FLAG_ONE_SHOT);
		AlarmManager am1 = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		am1.set(AlarmManager.RTC_WAKEUP, timeInMilliseconds5, pIntentFinal);
		// am.set(AlarmManager.RTC_WAKEUP, selectedDateTime.getTimeInMillis(),
		// pIntent);
		ApplicationHelper.preferences.edit()
				.putInt(ApplicationHelper.FIRST_ALARM_ID, firstAlarmID)
				.commit();
		Log.e("selected time", selectedDateTime.getTimeInMillis() + "");
		Log.e("current", "" + current);
		Log.e("nextt", "" + next);

		int ThirdAlarmID = (int) System.currentTimeMillis();
		
		 * ApplicationHelper.preferences.edit()
		 * .putString(ApplicationHelper.NEAPPOINTMENT_TIME,
		 * secondAlarm).commit();
		 
		Long timeInMilliseconds = null;
		// Schedule second notification
		if (next != -1) {
			SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
			Date mDate = null;
			try {
				Log.e("secondssssssssalarmm", secondAlarm);

				mDate = sdf1.parse(secondAlarm);
				timeInMilliseconds = mDate.getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			selectedDateTime.add(Calendar.HOUR_OF_DAY, current);
			if (next == 24) {
				selectedDateTime.add(Calendar.DATE, -1);
			} else {
				selectedDateTime.add(Calendar.HOUR_OF_DAY, -next);
			}
			intent.putExtra("meeting_after", next);
			Log.e("current", "" + current);
			Log.e("nextt", "" + next);
			// int secondAlarmID = (int) System.currentTimeMillis();
			pIntent = PendingIntent.getBroadcast(this, ThirdAlarmID, intent,
					PendingIntent.FLAG_ONE_SHOT);
			am.set(AlarmManager.RTC_WAKEUP, timeInMilliseconds, pIntent);
			Log.e("selected time11", selectedDateTime.getTimeInMillis() + "");
			ApplicationHelper.preferences.edit()
					.putInt(ApplicationHelper.THIRD_REMINDER, ThirdAlarmID)
					.commit();

			if (Integer.parseInt(reminderOne[0]) > Integer
					.parseInt(reminderOne2[0])) {
				ApplicationHelper.preferences
						.edit()
						.putString(ApplicationHelper.FIRST_TIME, reminderOne[0])
						.commit();
				ApplicationHelper.preferences
						.edit()
						.putString(ApplicationHelper.Second_TIME,
								reminderOne2[0]).commit();

			}

			else {
				ApplicationHelper.preferences
						.edit()
						.putString(ApplicationHelper.FIRST_TIME,
								reminderOne2[0]).commit();
				ApplicationHelper.preferences
						.edit()
						.putString(ApplicationHelper.Second_TIME,
								reminderOne[0]).commit();

			}

		}
	}*/

	OnDateSetListener mOnDateSetListener = new OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			Calendar selectedDate = Calendar.getInstance();
			selectedDate.set(Calendar.YEAR, year);
			selectedDate.set(Calendar.MONTH, monthOfYear);
			selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

			if (selectedDate.after(selectedDateTime)
					|| ApplicationHelper.isTodayDate(year, monthOfYear,
							dayOfMonth)) {
				// Selected date is after the current date or may be near to the
				// current date
				selectedDateTime.set(Calendar.YEAR, year);
				selectedDateTime.set(Calendar.MONTH, monthOfYear);
				selectedDateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				if (status == 0) {
					new TimePickerDialog(AppointmentActivity.this,
							mOnTimeSetListener,
							selectedDateTime.get(Calendar.HOUR_OF_DAY),
							selectedDateTime.get(Calendar.MINUTE), false)
							.show();
					status = 1;
				}

			} else {
				// Prompt user that the date is not valid
				Toast.makeText(AppointmentActivity.this,
						promptOnWrongDateSelection, Toast.LENGTH_LONG).show();
			}
		}
	};

	OnTimeSetListener mOnTimeSetListener = new OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			selectedDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			selectedDateTime.set(Calendar.MINUTE, minute);
			selectedDateTime.set(Calendar.SECOND, 00);
			Log.e("selectedd", "selectedd" + hourOfDay + " " + minute);

			SimpleDateFormat sdf = new SimpleDateFormat(
					ApplicationHelper.DATE_TIME_FORMAT);
			Button appointmentButton = (Button) findViewById(R.id.button_setAppointment);
			appointmentButton.setText(sdf.format(new Date(selectedDateTime
					.getTimeInMillis())));

			String details[] = appointmentButton.getText().toString()
					.split(" ");
			selectedDateTime.getTimeInMillis();

			Log.e("detailsss", "" + appointmentButton.getText().toString());
			for (int i = 0; i < details.length; i++)
				Log.e("detailsss", "" + details[i]);
			secondAlarm = details[3] + " " + details[4];
			Log.e("finalss valueee", "" + details[3]);
			firstAlarmString = details[3];
			secondAlarm = appointmentButton.getText().toString();
			
			
			ApplicationHelper.preferences.edit().putString(ApplicationHelper.LAST_APPOINTMENT_DATE_TIME, secondAlarm).commit();
			

			Log.e("VAlue","TIME"+ ApplicationHelper.preferences.edit().putString(ApplicationHelper.LAST_APPOINTMENT_DATE_TIME, secondAlarm).commit());
			Log.e("VAlGET","TIME"+ ApplicationHelper.preferences.getString(ApplicationHelper.LAST_APPOINTMENT_DATE_TIME, secondAlarm));
			// secondAlarm=selectedDateTime.getTimeInMillis();
			// selectedDateTime.set
			// Log.e("detailsss",""+secondAlarm);
		}
	};

}
