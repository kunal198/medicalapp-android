package com.gardenislandtech.eyeclinic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gardenislandtech.eyeclinic.adapters.MedicationTimeListAdapter;
import com.gardenislandtech.eyeclinic.adapters.MySpinnerAdapter;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

public class MedicationActivity extends TabActivity {

	private boolean isNew;
	private Calendar startDate;
	private SimpleDateFormat dateTimeFormat;

	private List<String> medicationTimes;
	private ListView listOfMedicationTimes;
	private MedicationTimeListAdapter adapter;

	private static String selectedMedicine;
	private String promptOnSelectionOfAlreadyScheduledMedication = "Medication has already scheduled";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getLayoutInflater().inflate(R.layout.activity_medication,
				(LinearLayout) findViewById(R.id.tab3));
		Log.e("finalss1", "finalss1");

		startDate = Calendar.getInstance();
		medicationTimes = new ArrayList<String>();
		isNew = getIntent().getExtras().getBoolean("is_new");
		dateTimeFormat = new SimpleDateFormat(ApplicationHelper.DATE_FORMAT);
		this.getResources().getDimension(R.dimen.doctor_1);
		Log.e("detaillss",
				"" + this.getResources().getDimension(R.dimen.spinner_size));

		// spinner.setAdapter(spinnerAdapter);
		// ArrayAdapter<Medicine> spinnerAdapter = new
		// ArrayAdapter<Medicine>(this, android.R.layout.simple_spinner_item,
		// ApplicationHelper.database.getMedicines());
		// spinner.setAdapter(adapter);
		// ArrayAdapter<CharSequence> spinnerAdapter =
		// ArrayAdapter.createFromResource(this,
		// R.array.medicine_array, android.R.layout.simple_spinner_item);
		Spinner spinner = (Spinner) findViewById(R.id.myFirstSpinner);

		// this.TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.spinner_size));
		final MySpinnerAdapter spinnerAdapter = new MySpinnerAdapter(this,
				android.R.layout.simple_list_item_2,
				ApplicationHelper.database.getMedicines());
		spinner.setAdapter(spinnerAdapter);
		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View child,
					int position, long id) {
				selectedMedicine = spinnerAdapter.getItem(position).get_name();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		if (!isNew) {
			TextView newMedicationLabel = (TextView) findViewById(R.id.textView_newMedicationLabel);
			newMedicationLabel.setText("Update Medication");

			Medication m = ApplicationHelper.database
					.getMedicationById(getIntent().getExtras().getLong(
							"caller_id"));

			// spinner.setSelection(spinnerAdapter.getPosition(selectedMedicine
			// = m.get_name()));
			// spinner.setSelection(spinnerAdapter.getPosition(m));

			Button startDateButton = (Button) findViewById(R.id.button_startDate);
			startDateButton.setText(m.get_start_date());
			startDateButton.setEnabled(false);

			Button endDateButton = (Button) findViewById(R.id.button_endDate);
			endDateButton.setText(m.get_end_date() != null ? m.get_end_date()
					: "Never");
			endDateButton.setEnabled(false);

			Button addFrequency = (Button) findViewById(R.id.button_frequency);
			addFrequency.setEnabled(false);

			Button saveButton = (Button) findViewById(R.id.button_saveMedicine);
			saveButton.setEnabled(false);

			for (int i = 0; i < m.get_medication_times().size(); i++)
				medicationTimes.add(m.get_medication_times().get(i)
						.get_medication_time());
		}

		adapter = new MedicationTimeListAdapter(medicationTimes);
		listOfMedicationTimes = (ListView) findViewById(R.id.listView_medicationTimes);
		listOfMedicationTimes.setAdapter(adapter);

		setupTabOne();
		setupTabTwo(msg);
		setupTabFour();
		setupTabFive();
	}

	public void showDate(final View v) {
		final Calendar calendarOne = Calendar.getInstance();
		DatePickerDialog dpd = new DatePickerDialog(this,
				new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Calendar calendarTwo = Calendar.getInstance();
						calendarTwo.set(Calendar.YEAR, year);
						calendarTwo.set(Calendar.MONTH, monthOfYear);
						calendarTwo.set(Calendar.DAY_OF_MONTH, dayOfMonth);

						if (calendarTwo.before(calendarOne)) {
							Toast.makeText(getApplicationContext(),
									"Date is not valid!!!", Toast.LENGTH_LONG)
									.show();
							return;
						}

						dateTimeFormat
								.applyPattern(ApplicationHelper.DATE_FORMAT);

						if (v.getTag().equals("1")) {
							startDate = calendarTwo;
							((Button) v).setText(dateTimeFormat
									.format(calendarTwo.getTime()));
							Button endDate = (Button) findViewById(R.id.button_endDate);
							endDate.setText("Never");
						} else if (v.getTag().equals("2")) {
							if (calendarTwo.before(startDate)) {
								Toast.makeText(
										getApplicationContext(),
										"End date must after or same as start date.",
										Toast.LENGTH_LONG).show();
							} else {
								((Button) v).setText(dateTimeFormat
										.format(calendarTwo.getTime()));
							}
						}
					}
				}, calendarOne.get(Calendar.YEAR), calendarOne
						.get(Calendar.MONTH), calendarOne
						.get(Calendar.DAY_OF_MONTH));
		dpd.show();
	}

	public void setTime(View v) {
		final Calendar calendar = Calendar.getInstance();
		TimePickerDialog tpd = new TimePickerDialog(this,
				new OnTimeSetListener() {

					@Override
					public void onTimeSet(TimePicker view, int hourOfDay,
							int minute) {
						calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
						calendar.set(Calendar.MINUTE, minute);
						dateTimeFormat
								.applyPattern(ApplicationHelper.TIME_FORMAT);
						String time = dateTimeFormat.format(calendar.getTime());

						if (!medicationTimes.contains(time)) {
							medicationTimes.add(time);
							// sortMedicationList(hourOfDay, minute);
							adapter.notifyDataSetChanged();
						} else {
							Toast.makeText(getApplicationContext(),
									"Time already added.", Toast.LENGTH_LONG)
									.show();
						}
					}
				}, calendar.get(Calendar.HOUR_OF_DAY), calendar
						.get(Calendar.MINUTE), false);
		tpd.show();
	}

	public void onCrossButtonClick(View v) {
		int position = (Integer) v.getTag();
		medicationTimes.remove(position);
		adapter.notifyDataSetChanged();
	}

	public void onSaveButtonClick(View v) {
		if (ApplicationHelper.database
				.checkIfMedicationIsAlreadyScheduled(selectedMedicine)) {
			Toast.makeText(this, promptOnSelectionOfAlreadyScheduledMedication,
					Toast.LENGTH_SHORT).show();
			return;
		}

		Button startButton = (Button) findViewById(R.id.button_startDate);
		String __StartDate = startButton.getText().toString();

		if (__StartDate.equals("Today")) {
			dateTimeFormat.applyPattern(ApplicationHelper.DATE_FORMAT);
			__StartDate = dateTimeFormat.format(startDate.getTime());
		}

		Button endButton = (Button) findViewById(R.id.button_endDate);
		String __EndDate = endButton.getText().toString();

		if (__EndDate.equals("Never"))
			__EndDate = null;

		List<MedicationTime> medicationTimesList = new ArrayList<MedicationTime>();

		if (medicationTimes.size() > 0) {
			for (int i = 0; i < medicationTimes.size(); i++) {
				MedicationTime md = new MedicationTime();
				md.set_medication_time(medicationTimes.get(i));
				md.set_medication_date(__StartDate);
				medicationTimesList.add(md);
			}
		} else {
			Toast.makeText(this, "Please add atleast one frequency",
					Toast.LENGTH_SHORT).show();
			return;
		}

		Medication m = new Medication();
		m.set_name(selectedMedicine);
		m.set_start_date(__StartDate);
		m.set_end_date(__EndDate);
		m.set_medication_times(medicationTimesList);

		if (isNew) {
			m.set_id(ApplicationHelper.database.insertMedication(m));
			Log.e("savedd meditationn", "" + m.get_id());
			try {
				checkForNextNotification(getApplicationContext(), __StartDate,
						__EndDate, medicationTimes.get(0), 0);
				Toast.makeText(this, "Saved!!!", Toast.LENGTH_SHORT).show();
				ApplicationHelper.medicationDataSet.add(m);
				ApplicationHelper.medicationAdapter.notifyDataSetChanged();
				setResult(RESULT_OK);
				finish();

				Intent goback = new Intent(MedicationActivity.this,
						MainActivity.class);
				startActivity(goback);

			} catch (ParseException e) {
				e.printStackTrace();
				Toast.makeText(getApplicationContext(),
						"Medication is being saved.", Toast.LENGTH_SHORT)
						.show();
			}
		} else {
			// update medication in database
		}
	}

	// private void sortMedicationList(int h, int m) {
	// int[] times = new int[medicationTimes.size()];
	// for (int i = 0; i < medicationTimes.size(); i++) {
	// StringBuilder sb = new StringBuilder(medicationTimes.get(i));
	// sb = sb.deleteCharAt(2);
	// int integerValue = Integer.valueOf(sb.toString());
	// times[i] = integerValue;
	// }
	//
	// if (times.length > 0) Arrays.sort(times);
	//
	// for (int i = 0; i < times.length; i++) {
	// StringBuilder sb = new StringBuilder("");
	// sb = sb.append(times[i]);
	// sb.insert(h>=0 && h<10?1:2, ':');
	// medicationTimes.remove(i);
	// medicationTimes.add(i, sb.toString());
	// }
	// }

	public static boolean checkForNextNotification(Context context,
			String start_date, String end_date, String time, long id)
			throws ParseException {
		boolean result;
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(ApplicationHelper.TIME_FORMAT);
		GregorianCalendar gc = new GregorianCalendar();
		Date date = sdf.parse(time);
		gc.setTime(date);
		Calendar nnt = Calendar.getInstance();
		nnt.set(Calendar.HOUR_OF_DAY, gc.get(Calendar.HOUR_OF_DAY));
		nnt.set(Calendar.MINUTE, gc.get(Calendar.MINUTE));
		nnt.set(Calendar.SECOND, 0);

		// Check for end date.
		Calendar endDate = null;
		if (end_date != null) {
			sdf.applyPattern(ApplicationHelper.DATE_FORMAT);
			date = sdf.parse(end_date);
			gc.setTime(date);
			endDate = Calendar.getInstance();
			endDate.set(Calendar.YEAR, gc.get(Calendar.YEAR));
			endDate.set(Calendar.MONTH, gc.get(Calendar.MONTH));
			endDate.set(Calendar.DAY_OF_MONTH, gc.get(Calendar.DAY_OF_MONTH));
			endDate.set(Calendar.MINUTE, 59);
			endDate.set(Calendar.HOUR_OF_DAY, 23);
		}

		// Check if notification time is before current time... then schedule it
		// for next day.
		if (nnt.before(Calendar.getInstance())) {
			nnt.add(Calendar.DAY_OF_MONTH, 1);
			if (endDate != null) {
				if (nnt.after(endDate)) {
					result = false;
				} else {
					scheduleNotification(context, nnt);
					result = true;

					if (id != 0) {
						// update medication time here
						sdf.applyPattern(ApplicationHelper.DATE_FORMAT);
						String __date = sdf.format(nnt.getTime());
						ApplicationHelper.database.updateMedicationTime(id,
								__date);
					}
				}
			} else {
				scheduleNotification(context, nnt);
				result = true;

				if (id != 0) {
					// update medication time here
					sdf.applyPattern(ApplicationHelper.DATE_FORMAT);
					String __date = sdf.format(nnt.getTime());
					ApplicationHelper.database.updateMedicationTime(id, __date);
				}
			}
		} else {
			if (endDate != null) {
				if (nnt.after(endDate)) {
					result = false;
				} else {
					scheduleNotification(context, nnt);
					result = true;

					if (id != 0) {
						// update medication time here
						sdf.applyPattern(ApplicationHelper.DATE_FORMAT);
						String __date = sdf.format(nnt.getTime());
						ApplicationHelper.database.updateMedicationTime(id,
								__date);
					}
				}
			} else {
				scheduleNotification(context, nnt);
				result = true;

				if (id != 0) {
					// update medication time here
					sdf.applyPattern(ApplicationHelper.DATE_FORMAT);
					String __date = sdf.format(nnt.getTime());
					ApplicationHelper.database.updateMedicationTime(id, __date);
				}
			}
		}

		return result;

		// -------------------------------------------------------------------------------
		// SimpleDateFormat sdf = new SimpleDateFormat();
		//
		// // This block will find the start date
		// sdf.applyPattern(AppConstants.DATE_FORMAT);
		// Calendar osd = Calendar.getInstance();
		// osd.setTime(sdf.parse(start_date));
		//
		// // Setting Year, month, date.
		// Calendar nsd = Calendar.getInstance();
		// nsd.set(Calendar.YEAR, osd.get(Calendar.YEAR));
		// nsd.set(Calendar.MONTH, osd.get(Calendar.MONTH));
		// nsd.set(Calendar.DAY_OF_MONTH, osd.get(Calendar.DAY_OF_MONTH));
		//
		// // Setting Hour, minute.
		// sdf.applyPattern(AppConstants.TIME_FORMAT);
		// osd.setTime(sdf.parse(time));
		// nsd.set(Calendar.HOUR_OF_DAY, osd.get(Calendar.HOUR_OF_DAY));
		// nsd.set(Calendar.MINUTE, osd.get(Calendar.MINUTE));
		//
		// // if not todays date it will assign to 00 start time.
		// // if (!AppointmentActivity.isTodayDate(nsd.get(Calendar.YEAR),
		// nsd.get(Calendar.MONTH), nsd.get(Calendar.DAY_OF_MONTH))) {
		// // nsd.set(Calendar.MINUTE, 00);
		// // nsd.set(Calendar.HOUR_OF_DAY, 00);
		// // }
		//
		// // This block will find the notification time
		// sdf.applyPattern(AppConstants.TIME_FORMAT);
		// Calendar ont = Calendar.getInstance();
		// ont.setTime(sdf.parse(time));
		// Calendar nnt = Calendar.getInstance();
		// nnt.set(Calendar.MINUTE, ont.get(Calendar.MINUTE));
		// nnt.set(Calendar.HOUR_OF_DAY, ont.get(Calendar.HOUR_OF_DAY));
		// nnt.set(Calendar.DAY_OF_MONTH, nsd.get(Calendar.DAY_OF_MONTH));
		// nnt.set(Calendar.MONTH, nsd.get(Calendar.MONTH));
		// nnt.set(Calendar.YEAR, nsd.get(Calendar.YEAR));
		//
		// if (end_date != null) {
		// // This block will find the end date
		// sdf.applyPattern(AppConstants.DATE_FORMAT);
		// Calendar oed = Calendar.getInstance();
		// oed.setTime(sdf.parse(end_date));
		// Calendar ned = Calendar.getInstance();
		// ned.set(Calendar.MINUTE, 59);
		// ned.set(Calendar.HOUR_OF_DAY, 23);
		// ned.set(Calendar.YEAR, oed.get(Calendar.YEAR));
		// ned.set(Calendar.MONTH, oed.get(Calendar.MONTH));
		// ned.set(Calendar.DAY_OF_MONTH, oed.get(Calendar.DAY_OF_MONTH));
		//
		// if (nnt.after(nsd) && nnt.before(ned)) {
		// scheduleNotification(context, nnt);
		// result = true;
		// } else {
		// result = false;
		// }
		// } /*else if (nnt.after(nsd)) {
		// scheduleNotification(context, nnt);
		// result = true;
		// } */else {
		// scheduleNotification(context, nnt);
		// result = true;
		// }
		//
		// return result;
		// -------------------------------------------------------------------------------
	}

	private static void scheduleNotification(Context ctx, Calendar cal) {
		Intent intent = new Intent();
		intent.putExtra("medicine_name", selectedMedicine);
		intent.setAction("com.gardenislandtech.eyeclinic.receivers.MedicationsReceiver");
		PendingIntent pIntent = PendingIntent.getBroadcast(ctx,
				(int) System.currentTimeMillis(), intent,
				PendingIntent.FLAG_ONE_SHOT);
		AlarmManager am = (AlarmManager) ctx
				.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pIntent);
	}

}
