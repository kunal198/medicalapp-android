package com.gardenislandtech.eyeclinic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.gardenislandtech.eyeclinic.adapters.MedicationListAdapter;
import com.gardenislandtech.eyeclinic.fragments.FragmentAboutDoc;
import com.gardenislandtech.eyeclinic.fragments.FragmentDocsName;
import com.gardenislandtech.eyeclinic.fragments.FragmentGrid;
import com.gardenislandtech.eyeclinic.fragments.FragmentHome;
import com.gardenislandtech.eyeclinic.fragments.FragmentLocation;
import com.gardenislandtech.eyeclinic.fragments.FragmentNews;
import com.gardenislandtech.eyeclinic.fragments.FragmentPrescription;
import com.gardenislandtech.eyeclinic.fragments.FragmentTool;
import com.gardenislandtech.eyeclinic.fragments.FragmentVideo;
import com.gardenislandtech.eyeclinic.utils.ApplicationHelper;

public class TabActivity extends FragmentActivity {

	private TabHost mTabHost;
	private MenuItem mi_scheduleNewDrug;
	private MenuItem mi_addPrescriptionImage;
	String msg, prefvalue;
	private static CountDownTimer cdTimer;
	String month, date, year, time, amPm, appointmentMsg, apMessage;
	private SharedPreferences sharedpreferences;
	private SharedPreferences.Editor editor;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab);

		sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);

		Log.e("detailss", "fgfgf" + MainActivity.fa);
		if (MainActivity.fa != null)
			MainActivity.fa.finish();

		int firstColor = Color.parseColor("#9B988E");
		int secondColor = Color.parseColor("#453F2D");
		GradientDrawable gd = new GradientDrawable(Orientation.TOP_BOTTOM, new int[] { firstColor, secondColor });

		if (Build.VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
			// getActionBar().setBackgroundDrawable(gd);
		} else {
			View v = (View) getWindow().findViewById(android.R.id.title).getParent();
			v.setBackgroundDrawable(gd);
		}

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		// Tab 1
		TabSpec tab1 = mTabHost.newTabSpec("tab1");
		tab1.setIndicator(LayoutInflater.from(this).inflate(R.layout.tab1_indicator, null));
		tab1.setContent(R.id.tab1);
		mTabHost.addTab(tab1);

		// Tab 2
		TabSpec tab2 = mTabHost.newTabSpec("tab2");
		tab2.setIndicator(LayoutInflater.from(this).inflate(R.layout.tab2_indicator, null));
		tab2.setContent(R.id.tab2);
		mTabHost.addTab(tab2);

		// Tab 3
		TabSpec tab3 = mTabHost.newTabSpec("tab3");
		tab3.setIndicator(LayoutInflater.from(this).inflate(R.layout.tab3_indicator, null));
		tab3.setContent(R.id.tab3);
		mTabHost.addTab(tab3);

		// Tab 4
		TabSpec tab4 = mTabHost.newTabSpec("tab4");
		tab4.setIndicator(LayoutInflater.from(this).inflate(R.layout.tab4_indicator, null));
		tab4.setContent(R.id.tab4);
		mTabHost.addTab(tab4);

		// Tab 5
		TabSpec tab5 = mTabHost.newTabSpec("tab5");
		tab5.setIndicator(LayoutInflater.from(this).inflate(R.layout.tab5_indicator, null));
		tab5.setContent(R.id.tab5);
		mTabHost.addTab(tab5);

		TabSpec tab6 = mTabHost.newTabSpec("tab6");
		tab6.setIndicator(LayoutInflater.from(this).inflate(R.layout.tab6_indicator, null));
		tab6.setContent(R.id.tab6);
		mTabHost.addTab(tab6);

		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				int t = 5;
				Log.e("asdsd", "dsds" + tabId);
				FragmentManager fm = getSupportFragmentManager();
				fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				if (tabId.equals("tab6")) {
					Log.e("asdsd", "dsds" + tabId);
					FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
					FragmentNews fNews = new FragmentNews();
					ft.replace(R.id.tab6, fNews, "fragment_news");
					ft.commit();
				}

				if (ApplicationHelper.CURRENT_TAB == 0 || ApplicationHelper.CURRENT_TAB == 4) {
					FragmentManager fm1 = getSupportFragmentManager();
					fm1.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}

				if (ApplicationHelper.CURRENT_TAB == 4)
					if (mi_addPrescriptionImage != null)
						mi_addPrescriptionImage.setVisible(false);

				ApplicationHelper.CURRENT_TAB = mTabHost.getCurrentTab();

				if (ApplicationHelper.CURRENT_TAB == 2) {
					if (mi_scheduleNewDrug != null)
						mi_scheduleNewDrug.setVisible(true);
				} else {
					if (mi_scheduleNewDrug != null)
						mi_scheduleNewDrug.setVisible(false);
				}

			}
		});
		
	}

	@Override
	protected void onStart() {
		super.onStart();
		FragmentManager fm = getSupportFragmentManager();
		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		mTabHost.setCurrentTab(ApplicationHelper.CURRENT_TAB);
	}

	@Override
	protected void onResume() {
		super.onResume();

		
		
		
		if (ApplicationHelper.CURRENT_TAB == 1) {
			setupTabTwo(msg);
		} else if (ApplicationHelper.CURRENT_TAB == 2) {
			ApplicationHelper.medicationDataSet = ApplicationHelper.database.getMedications();
			ApplicationHelper.medicationAdapter = new MedicationListAdapter(ApplicationHelper.medicationDataSet);
		}

		String abc=getApointmentMsg();
		Log.e("appointmentMsg00", "TIME" + appointmentMsg);
		TextView missedTextView = (TextView) findViewById(R.id.textView_missedAppointment);
		missedTextView.setVisibility(View.VISIBLE);
		missedTextView.setText(abc);
		
		
		
		TextView textView_appointmentTime=(TextView)findViewById(R.id.textView_appointmentTime);
		Log.e("ResText","TIME"+time);
		
		
		if(!abc.equals("Appointment Soon"))
			missedTextView.setTextColor(Color.RED);
		Log.e("apMessage", "TIME" + abc);
		String newAppointmentDateTime = ApplicationHelper.preferences.getString(ApplicationHelper.NEAPPOINTMENT_TIME,
				null);
		 Log.e("ResTime","TIME"+ newAppointmentDateTime);
		try {if(newAppointmentDateTime.equals(" ")){
			RelativeLayout rl = (RelativeLayout)
				findViewById(R.id.relativeLayout_nextAppointmentShowcase);
				 rl.setVisibility(View.GONE);
				 
				 Button button_appointmentSetter=(Button)findViewById(R.id.button_appointmentSetter);
				 button_appointmentSetter.setVisibility(View.VISIBLE);
				 
				 
				  
				  
				  
				 
		}
		
		else{
			RelativeLayout rl = (RelativeLayout)
					findViewById(R.id.relativeLayout_nextAppointmentShowcase);
					 rl.setVisibility(View.VISIBLE);
		}
		}
		catch(Exception e){}
		/*SharedPreferences sharedPreferences = this.getSharedPreferences("appointment_msg", Context.MODE_APPEND);
	        sharedPreferences.edit().putString("app_msg", null).commit();*/
	        
	}
	 public String getApointmentMsg() {
	        SharedPreferences sharedPreferences = this.getSharedPreferences("appointment_msg", Context.MODE_APPEND);
	        appointmentMsg=sharedPreferences.getString("app_msg", "");
	       return appointmentMsg;
	    }
	public void setAppointmentMsg(String msg) {
		if(msg!=null)
		{
	        SharedPreferences sharedPreferences = this.getSharedPreferences("appointment_msg", Context.MODE_APPEND);
	        SharedPreferences.Editor editor = sharedPreferences.edit();
	   
	        editor.putString("app_msg", msg);
	        editor.commit();
		}

	    }


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Fragment f = getSupportFragmentManager().findFragmentByTag("fragment_prescription");
		if (f != null)
			mi_addPrescriptionImage.setVisible(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tab, menu);
		mi_scheduleNewDrug = menu.findItem(R.id.action_addNewDrug);
		mi_addPrescriptionImage = menu.findItem(R.id.action_addNewPrescription);
		mi_addPrescriptionImage.setVisible(false);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If Medication tab is currently selected then option will available
		// otherwise not.
		if (ApplicationHelper.CURRENT_TAB == 2) {
			mi_scheduleNewDrug.setVisible(true);
		} else {
			mi_scheduleNewDrug.setVisible(false);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_addNewDrug:
			Intent i = new Intent(this, MedicationActivity.class);
			i.putExtra("is_new", true);
			startActivityForResult(i, 44444);
			break;

		case R.id.action_addNewPrescription:
			FragmentPrescription fp = (FragmentPrescription) getSupportFragmentManager().findFragmentByTag(
					"fragment_prescription");
			fp.addNewPrescription();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 55555 && resultCode == RESULT_OK) {
			setupTabTwo(msg);
		} else if (requestCode == 44444 && resultCode == RESULT_OK) {
			if (ApplicationHelper.medicationDataSet.size() > 0) {
				TextView noMedication = (TextView) findViewById(R.id.textView_noMedicationsYet);
				noMedication.setVisibility(View.GONE);
			}
		}
	}/*ApplicationHelper.preferences
	.edit()
	.putString(ApplicationHelper.LAST_APPOINTMENT_DATE_TIME, secondAlarm)
	.commit();*/

	// Protected Class Methods
	protected void setupTabOne() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		FragmentHome homeFragment = new FragmentHome();
		ft.add(R.id.tab1, homeFragment, "fragment_home");
		ft.commit();
	}

	protected void setupTabTwo(String msg) {
		this.msg = msg;
		Log.e("msg", "TIME" + msg);

		View v = getLayoutInflater().inflate(R.layout.layout_tab2, (LinearLayout) this.findViewById(R.id.tab2));

		
		String getLst=ApplicationHelper.preferences.getString(ApplicationHelper.LAST_APPOINTMENT, "");
		Log.e("getLst","TIME" +getLst);
		
		TextView textView_lastAppointment=(TextView)findViewById(R.id.textView_lastAppointment);
		//textView_lastAppointment.setText(getLst);
		/*if(getLst.equals("")){
			textView_lastAppointment.setText(getLst);
		}
		else */
		if(getLst!=null){
		//	try{
			Log.e("VISitf", "TIME"+getLst);
			textView_lastAppointment.setVisibility(View.VISIBLE);
		textView_lastAppointment.setText(getLst);//}catch(Exception e){}
		
		Log.e("VISitf", "TIME"+textView_lastAppointment.getText().toString());
		}
		else{
			Log.e("VISitel", "TIME");
			textView_lastAppointment.setText("Not set Yet");
		}
		
		
		
		
		/*TextView lastAppointment = (TextView) v.findViewById(R.id.textView_lastAppointment);
		if(ApplicationHelper.preferences.getString(ApplicationHelper.LAST_APPOINTMENT_DATE_TIME, " ").equals(" "))
		{
		lastAppointment.setText(
				"Not set yet.");
		}
		
		else{
			lastAppointment.setText(ApplicationHelper.preferences.getString(ApplicationHelper.LAST_APPOINTMENT_DATE_TIME, " "));
		}*/
		setAppointmentMsg(msg);

		
		/*
		 * if(msg.equals(null)){ RelativeLayout rl = (RelativeLayout)
		 * findViewById(R.id.relativeLayout_nextAppointmentShowcase);
		 * 
		 * rl.setVisibility(View.GONE); }
		 */

		Log.e("messageINPr", "TIME" + msg);

		/*editor = sharedpreferences.edit();
		editor.putString(ApplicationHelper.APMESSAGE, "1");
		editor.putString(ApplicationHelper.MESSAGE_NOTI, msg);
		editor.commit();
 */
		// ApplicationHelper.preferences.edit()
		// .putString(ApplicationHelper.MESSAGE, msg).commit();

		// String message=
		
		/*try{
		if(apMessage.equals("1")){
			TextView missedTextView = (TextView) v.findViewById(R.id.textView_missedAppointment);
			missedTextView.setVisibility(View.VISIBLE);
			missedTextView.setText("Appointment Soon");
		}
		}catch(Exception e){}*/
		String newAppointmentDateTime = ApplicationHelper.preferences.getString(ApplicationHelper.NEAPPOINTMENT_TIME,
				null);

		// / TextView
		// appintmentTime=(TextView)findViewById(R.id.textView_appointmentDate);
		// Log.e("getDateTime","TIME"+newAppointmentDateTime);

		/*
		 * ApplicationHelper.preferences.edit().putString(ApplicationHelper. FirstNotification, msg).commit();
		 */
		/*
		 * Editor editor = preferences.edit(); editor.putString("key", "value"); editor.commit();
		 */

		// sharedpreferences=PreferenceManager.getDefaultSharedPreferences(TabActivity.this);
		// SharedPreferences.Editor editor = sharedpreferences.edit();
		//
		// editor.putString("MESSAGE", msg);
		//
		// editor.commit();

		Log.e("appointmentMsgRes", "TIME" + appointmentMsg);
		/*if (newAppointmentDateTime.equals(" ")) {
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativeLayout_nextAppointmentShowcase);

			rl.setVisibility(View.GONE);
		}
		else	*/if (appointmentMsg != null) {
			TextView missedTextView = (TextView) v.findViewById(R.id.textView_missedAppointment);
			missedTextView.setVisibility(View.VISIBLE);
			missedTextView.setText(appointmentMsg);
			
		}
		try {
			String[] separated = newAppointmentDateTime.split(" ");

			month = separated[0].trim();
			date = separated[1].trim();
			year = separated[2].trim();
			// String date=separated[3].trim();
			time = separated[4].trim();
			amPm = separated[5].trim();

			Log.e("month", "TIME" + month);
			Log.e("date", "TIME" + date);
			Log.e("year", "TIME" + year);

			Log.e("Dates", "TIME" + time);
			Log.e("AMPM", "TIME" + amPm);

		} catch (Exception e) {
		}

		// if (newAppointmentDateTime.equals(null)) {
		// RelativeLayout rl = (RelativeLayout)
		// findViewById(R.id.relativeLayout_nextAppointmentShowcase);
		//
		// rl.setVisibility(View.GONE);
		//
		// }
		Log.e("visible", "TIME" + newAppointmentDateTime);
		Log.e("amPmbe", "TIME" + amPm);
		TextView textView_appointmentTime=(TextView)findViewById(R.id.textView_appointmentTime);
		
		/*try{if (amPm.isEmpty()) {
			Log.e("VISIT","TIME");
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativeLayout_nextAppointmentShowcase);

			rl.setVisibility(View.GONE);
		
		}
		}
		catch(Exception e){}*/
		//else 
		
		 if (newAppointmentDateTime != null && newAppointmentDateTime!="") {
			
			 Log.e("VISIT", "TIME" + newAppointmentDateTime);
			
			Button appointmentSetterButton = (Button) findViewById(R.id.button_appointmentSetter);
			appointmentSetterButton.setVisibility(View.GONE);
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativeLayout_nextAppointmentShowcase);

			rl.setVisibility(View.VISIBLE);

			// SimpleDateFormat sdf = new
			// SimpleDateFormat(ApplicationHelper.DATE_TIME_FORMAT);
			// Date dateAndTime = null;
			// try {
			// dateAndTime = sdf.parse(newAppointmentDateTime);
			// } catch (ParseException e) {
			// e.printStackTrace();
			// }

			// sdf.applyPattern(ApplicationHelper.DATE_FORMAT);

			TextView appointmentDate = (TextView) findViewById(R.id.textView_appointmentDate);
			// appointmentDate.setText(newAppointmentDateTime);
			appointmentDate.setText(month + " " + date + " " + year);

			TextView appointmentTime = (TextView) findViewById(R.id.textView_appointmentTime);
			appointmentTime.setText(time + " " + amPm);
			TextView missedTextView = (TextView) v.findViewById(R.id.textView_missedAppointment);
			missedTextView.setVisibility(View.VISIBLE);

			missedTextView.setText(msg);

			Button clearDone = (Button) findViewById(R.id.button_appointmentDone);

			/*
			 * if(msg.equals("Missed Appointment")){ missedTextView.setTextColor(Color.RED); } else{
			 * missedTextView.setTextColor(Color.WHITE); }
			 */

			// sdf.applyPattern(ApplicationHelper.TIME_FORMAT);
			// TextView appointmentTime = (TextView)
			// findViewById(R.id.textView_appointmentTime);
			// appointmentTime.setText(sdf.format(dateAndTime));

			/*
			 * if (ApplicationHelper.preferences.getBoolean(ApplicationHelper. IS_APPOINTMENT_MISSED, false)) {
			 * TextView missedTextView = (TextView) v.findViewById(R.id.textView_missedAppointment);
			 * missedTextView.setVisibility(View.VISIBLE); } SharedPreferences preferences =
			 * this.getSharedPreferences(this.getPackageName(), Context.MODE_PRIVATE);
			 * if(preferences.getInt(ApplicationHelper.SECOND_ALARM_ID, 0) !=
			 * 0&&preferences.getInt(ApplicationHelper.FIRST_ALARM_ID, 0) !=
			 * 0&&preferences.getInt(ApplicationHelper.THIRD_REMINDER, 0) != 0) {} else if
			 * (preferences.getInt(ApplicationHelper.FIRST_ALARM_ID, 0) ==
			 * 0&&preferences.getInt(ApplicationHelper.SECOND_ALARM_ID, 0) !=
			 * 0&&preferences.getInt(ApplicationHelper.THIRD_REMINDER, 0) != 0) { TextView missedTextView =
			 * (TextView)v. findViewById(R.id.textView_missedAppointment);
			 * missedTextView.setVisibility(View.VISIBLE); Log.e("detailss1","details1");
			 * 
			 * } else if(preferences.getInt(ApplicationHelper.SECOND_ALARM_ID, 0) ==
			 * 0&&preferences.getInt(ApplicationHelper.FIRST_ALARM_ID, 0) ==
			 * 0&&preferences.getInt(ApplicationHelper.THIRD_REMINDER, 0) != 0) { TextView missedTextView =
			 * (TextView)v. findViewById(R.id.textView_missedAppointment);
			 * missedTextView.setVisibility(View.VISIBLE); Log.e("detailss2","details1"); } else { TextView
			 * missedTextView = (TextView) findViewById(R.id.textView_missedAppointment);
			 * missedTextView.setVisibility(View.VISIBLE); missedTextView.setText("Missed Appointment !");
			 * missedTextView.setTextColor(Color.RED); Log.e("detailss3","details1"); }
			 * 
			 * if (ApplicationHelper.preferences.getBoolean(ApplicationHelper. IS_APPOINTMENT_MISSED, false)) {
			 * TextView missedTextView = (TextView) findViewById(R.id.textView_missedAppointment);
			 * missedTextView.setVisibility(View.VISIBLE); } } else { Button setAppointmentButton = (Button)
			 * findViewById(R.id.button_appointmentSetter); setAppointmentButton.setVisibility(View.VISIBLE);
			 */
		
		}
		 else{
			 Log.e("VISIT", "TIME" + newAppointmentDateTime);
				RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativeLayout_nextAppointmentShowcase);

				rl.setVisibility(View.GONE);
			}
	}

	public void onAppointmentDoneButtonClick(View v) {

		ApplicationHelper.preferences.edit().putString(ApplicationHelper.NEAPPOINTMENT_TIME, " ").commit();
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativeLayout_nextAppointmentShowcase);
		rl.setVisibility(View.GONE);
		Button setAppointmentButton = (Button) findViewById(R.id.button_appointmentSetter);
		setAppointmentButton.setVisibility(View.VISIBLE);

		
		
		
		AppointmentActivity.apointmentCAncel();
		SharedPreferences sharedPreferences = this.getSharedPreferences("appointment_msg", Context.MODE_APPEND);
	        SharedPreferences.Editor editor = sharedPreferences.edit();
	   
	        editor.putString("app_msg", null);
	        editor.commit();
	}

	protected void setupTabThree() {
		View v = getLayoutInflater().inflate(R.layout.layout_tab3, (LinearLayout) this.findViewById(R.id.tab3));

		ListView simpleListView = (ListView) v.findViewById(R.id.simple_listview);
		simpleListView.setAdapter(ApplicationHelper.medicationAdapter);
		simpleListView.setOnItemLongClickListener(mOnItemLongClickListener);

		if (ApplicationHelper.medicationDataSet.size() == 0) {
			TextView noMedication = (TextView) findViewById(R.id.textView_noMedicationsYet);
			noMedication.setVisibility(View.VISIBLE);
		}
	}

	protected void setupTabFour() {
		getLayoutInflater().inflate(R.layout.layout_tab4, (LinearLayout) this.findViewById(R.id.tab4));

		String lensLastChangedDate = ApplicationHelper.preferences.getString("last_lens_changed_date",
				getString(R.string.no_lens_change));
		TextView lastTimeTextView = (TextView) findViewById(R.id.textView_lensLastChangedTime);
		lastTimeTextView.setText(lensLastChangedDate);

		String scheduledDate = ApplicationHelper.preferences.getString("date", null);
		if (scheduledDate != null) {
			SimpleDateFormat df = new SimpleDateFormat("D:M:yyyy:HH:mm");
			try {
				Date date = df.parse(scheduledDate);
				setCountDownForRemainingDays(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			TextView daysLeftTxtView = (TextView) findViewById(R.id.textView_daysLeft);
			daysLeftTxtView.setTextColor(Color.RED);
		}
	}

	protected void setupTabFive() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		FragmentTool fTools = new FragmentTool();
		ft.add(R.id.tab5, fTools, "fragment_tools");
		ft.commit();
	}

	protected void setupTabSix() {
		/*
		 * FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		 * 
		 * FragmentNews fNews = new FragmentNews(); ft.add(R.id.tab6, fNews, "fragment_news"); ft.commit();
		 */
	}

	// Button click events
	public void onLocationClick(View v) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);

		FragmentLocation fl = new FragmentLocation();
		ft.replace(R.id.tab1, fl, "fragment_location");
		ft.addToBackStack(null);
		ft.commit();
	}

	public void onCallClick(View v) {
		FragmentHome fh = (FragmentHome) getSupportFragmentManager().findFragmentByTag("fragment_home");
		fh.onCallClick(v);
	}

	public void onWebsiteClick(View v) {
		FragmentHome fh = (FragmentHome) getSupportFragmentManager().findFragmentByTag("fragment_home");
		fh.onWebsiteClick(v);
	}

	public void onWebsiteLink(View v) {
		FragmentHome fh = (FragmentHome) getSupportFragmentManager().findFragmentByTag("fragment_home");
		fh.onWebsiteLink(v);
	}

	public void onDoctorButtonClick(View v) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);

		FragmentDocsName fd = new FragmentDocsName();
		ft.replace(R.id.tab1, fd, "doctors");
		ft.addToBackStack(null);
		ft.commit();
	}

	public void onDirectionsClick(View v) {
		FragmentLocation fl = (FragmentLocation) getSupportFragmentManager().findFragmentByTag("fragment_location");
		fl.onDirectionsClick(v);
	}

	public void onContactClick(View v) {
		FragmentLocation fl = (FragmentLocation) getSupportFragmentManager().findFragmentByTag("fragment_location");
		fl.onContactClick(v);
	}

	public void onDoctorDetailsButtonClick(View v) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);

		int doctor_id = Integer.parseInt(v.getTag().toString());
		FragmentAboutDoc fad = new FragmentAboutDoc(doctor_id);
		ft.replace(R.id.tab1, fad, "about_doctors");
		ft.addToBackStack(null);
		ft.commit();
	}

	public void onAppointmentSetterButtonClick(View v) {
		Intent intent = new Intent(this, AppointmentActivity.class);
		startActivityForResult(intent, 55555);
		String oldAppointmentDateTime = ApplicationHelper.preferences.getString(
				ApplicationHelper.NEW_APPOINTMENT_DATE_TIME, null);

		ApplicationHelper.preferences.edit().putBoolean(ApplicationHelper.IS_APPOINTMENT_MISSED, false)
				.putString(ApplicationHelper.NEW_APPOINTMENT_DATE_TIME, null)
				.putString(ApplicationHelper.OLD_APPOINTMENT_DATE_TIME, oldAppointmentDateTime).commit();
	}// Today is appraisal day...so please dont spoil my day...do your work and
		// aslo let me

	/*
	 * Intent intent = new Intent(); intent.setAction(
	 * "com.gardenislandtech.eyeclinic.receivers.AppointmentsReceiver"); AlarmManager am = (AlarmManager)
	 * getSystemService(Context.ALARM_SERVICE);
	 * 
	 * int id = ApplicationHelper.preferences.getInt(ApplicationHelper.FIRST_ALARM_ID, 0); if (id != 0) { PendingIntent
	 * firstPendingIntent = PendingIntent .getBroadcast(this, id, intent, PendingIntent.FLAG_ONE_SHOT);
	 * am.cancel(firstPendingIntent); firstPendingIntent.cancel(); ApplicationHelper
	 * .preferences.edit().putInt(ApplicationHelper.FIRST_ALARM_ID, 0).commit(); }
	 * 
	 * id = ApplicationHelper.preferences.getInt(ApplicationHelper.SECOND_ALARM_ID, 0); if (id != 0) { PendingIntent
	 * secondPendingIntent = PendingIntent .getBroadcast(this, id, intent, PendingIntent.FLAG_ONE_SHOT);
	 * am.cancel(secondPendingIntent); secondPendingIntent.cancel(); ApplicationHelper
	 * .preferences.edit().putInt(ApplicationHelper.SECOND_ALARM_ID, 0).commit(); }
	 * 
	 * String oldAppointmentDateTime = ApplicationHelper.preferences
	 * .getString(ApplicationHelper.NEW_APPOINTMENT_DATE_TIME, null);
	 * 
	 * ApplicationHelper.preferences.edit() .putBoolean(ApplicationHelper.IS_APPOINTMENT_MISSED, false)
	 * .putString(ApplicationHelper.NEW_APPOINTMENT_DATE_TIME, null)
	 * .putString(ApplicationHelper.OLD_APPOINTMENT_DATE_TIME, oldAppointmentDateTime).commit();
	 * 
	 * Intent i = getIntent(); finish(); startActivity(i);
	 */
	// }

	public void onTakeMedicineButtonClick(View v) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(ApplicationHelper.TIME_FORMAT);
		String time = sdf.format(c.getTime());
		long id = ((Number) v.getTag()).longValue();
		updateMedicationLastTakenTimeInDatabase(id, time);
	}

	public void onSkipMedicineButtonClick(View v) {
		long id = ((Number) v.getTag()).longValue();
		updateMedicationLastTakenTimeInDatabase(id, null);
	}

	private void updateMedicationLastTakenTimeInDatabase(long medicationId, String lastTakenTime) {
		// checking for new notification. If all medication times should reset
		// or not.
		String time = null;
		Medication m = null;
		boolean timeShouldReset = false;
		boolean isNextNotificationPossible = false;

		for (int i = 0; i < ApplicationHelper.medicationDataSet.size(); i++) {
			if (ApplicationHelper.medicationDataSet.get(i).get_id() == medicationId) {
				m = ApplicationHelper.medicationDataSet.get(i);
				for (int j = 0; j < m.get_medication_times().size(); j++) {
					if (m.get_medication_times().get(j).get_marked_finish() == 0) {
						if (j == m.get_medication_times().size() - 1) {
							timeShouldReset = true;
							time = m.get_medication_times().get(0).get_medication_time();
						} else {
							timeShouldReset = false;
							time = m.get_medication_times().get(j + 1).get_medication_time();
						}
						break;
					}
				}
				break;
			}
		}

		try {
			if (m != null)
				isNextNotificationPossible = MedicationActivity.checkForNextNotification(this, m.get_start_date(),
						m.get_end_date(), time, medicationId);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (isNextNotificationPossible) {
			ApplicationHelper.database.updateLastTakenTime(medicationId, lastTakenTime, timeShouldReset);
		} else {
			ApplicationHelper.database.deleteMedication(medicationId);
		}

		Intent i = getIntent();
		finish();
		startActivity(i);
	}

	public void setLensDisposalPeriod(View v) {
		final Calendar c = Calendar.getInstance();
		CharSequence[] chSeq = new CharSequence[] { "Daily", "2 Weeks", "Monthly", "Quarterly" };

		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Pick an option").setItems(chSeq, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case 0:
					c.add(Calendar.DAY_OF_YEAR, 1);
					ApplicationHelper.preferences.edit().putString("change_lens_period", "Daily").commit();
					break;

				case 1:
					c.add(Calendar.WEEK_OF_YEAR, 2);
					ApplicationHelper.preferences.edit().putString("change_lens_period", "2 Weeks").commit();
					break;

				case 2:
					c.add(Calendar.MONTH, 1);
					ApplicationHelper.preferences.edit().putString("change_lens_period", "Monthly").commit();
					break;

				case 3:
					c.add(Calendar.MONTH, 3);
					ApplicationHelper.preferences.edit().putString("change_lens_period", "Quarterly").commit();
					break;
				}

				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeZone(c.getTimeZone());
				gc.setTimeInMillis(c.getTimeInMillis() - System.currentTimeMillis());
				int months = gc.get(Calendar.MONTH);
				int days = gc.get(Calendar.DAY_OF_MONTH) - 1;
				int hours = gc.get(Calendar.HOUR_OF_DAY);
				int minutes = gc.get(Calendar.MINUTE);
				int seconds = gc.get(Calendar.SECOND);
				Log.i("TIME", months + " " + days + " " + hours + " " + minutes + " " + seconds);

				Date date = new Date(c.getTimeInMillis() - c.getTimeZone().getRawOffset());
				setCountDownForRemainingDays(date);
			}
		}).show();
	}

	public void onChangeLensesClick(View v) {

		String period = ApplicationHelper.preferences.getString("change_lens_period", null);
		TextView daysLeftTxtView = (TextView) findViewById(R.id.textView_daysLeft);
		

		if (period != null) {
			daysLeftTxtView.setVisibility(v.VISIBLE);

			Calendar c = Calendar.getInstance();
			Date d = new Date(c.getTimeInMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("MMM-d-yyyy");
			TextView lastChangedTime = (TextView) findViewById(R.id.textView_lensLastChangedTime);
			lastChangedTime.setText(sdf.format(d));
			ApplicationHelper.preferences.edit().putString("last_lens_changed_date", sdf.format(d)).commit();

			if (period.equals("Daily")) {
				c.add(Calendar.DAY_OF_YEAR, 1);
			} else if (period.equals("2 Weeks")) {
				c.add(Calendar.WEEK_OF_YEAR, 2);
			} else if (period.equals("Monthly")) {
				c.add(Calendar.MONTH, 1);
			} else if (period.equals("Quarterly")) {
				c.add(Calendar.MONTH, 3);
			}

			d = new Date(c.getTimeInMillis() - c.getTimeZone().getRawOffset());
			setCountDownForRemainingDays(d);
		} else {
			Toast.makeText(this, "Please set frequency first...", Toast.LENGTH_SHORT).show();
			return;
		}
	}

	public void orderOnline(View v) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW);
		browserIntent.setData(Uri.parse("http://www.ecckauai.com/onlineOptical.php"));
		startActivity(browserIntent);
	}

	// ApplicationHelper.preferences.edit()
	// .putInt(ApplicationHelper.FIRST_REMINDER, firstReminder =
	// newVal).commit();

	public void orderCancel(View v) {
		// ApplicationHelper.preferences.edit().clear().commit();
		// Button button_cancel=(Button)findViewById(R.id.button_cancel);
		/*
		 * SharedPreferences preferences = this.getSharedPreferences("last_lens_changed_date", Context.MODE_PRIVATE);
		 * preferences.edit().remove("last_lens_changed_date").commit();
		 */

		ApplicationHelper.preferences.edit().putString("last_lens_changed_date", null).commit();
		TextView daysLeftTxtView = (TextView) findViewById(R.id.textView_daysLeft);
		daysLeftTxtView.setVisibility(v.GONE);

	}

	private void setCountDownForRemainingDays(Date date) {
		final String scheduledCountDownDate = "date";
		final TextView txtView = (TextView) findViewById(R.id.textView_daysLeft);
		txtView.setTextColor(Color.BLACK);

		if (cdTimer != null) {
			cdTimer.cancel();
			ApplicationHelper.preferences.edit().remove(scheduledCountDownDate).commit();
		}

		long millis = date.getTime() - System.currentTimeMillis();
		cdTimer = new CountDownTimer(millis, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(millisUntilFinished);
				// int seconds = (int) (millisUntilFinished / 1000);
				// int minutes = (int) (millisUntilFinished / (1000 * 60));
				// int hours = (int) (millisUntilFinished / (1000 * 60 * 60));
				// int days = (int) (millisUntilFinished / (1000 * 60 * 60 *
				// 24));

				// int months = (int) (millisUntilFinished / (1000 * 60 * 60 *
				// 24 * 30));
				// long days =
				// TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
				// long hours =
				// TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
				// txtView.setText(days + " Day(s) " + hours + " Hour(s) "
				// + minutes + " Minute(s) " + seconds + " Second(s) Left");
				int months = gc.get(Calendar.MONTH);
				int days = gc.get(Calendar.DAY_OF_MONTH) - 1;
				int hours = gc.get(Calendar.HOUR_OF_DAY);
				int minutes = gc.get(Calendar.MINUTE);
				int seconds = gc.get(Calendar.SECOND);
				// Log.i("TIME", months + " " + days + " " + hours + " " +
				// minutes + " " + seconds);
				txtView.setText(months + " Month(s) " + days + " Day(s) " + hours + " Hour(s) " + minutes
						+ " Minute(s) " + seconds + " Second(s) Left");
			}

			@Override
			public void onFinish() {
				txtView.setTextColor(Color.RED);
				txtView.setText("Time to change lenses!");
				ApplicationHelper.preferences.edit().remove(scheduledCountDownDate).commit();

				// Schedule Notification that Lens has expired
				Intent i = new Intent();
				PendingIntent pIntent = PendingIntent.getActivity(TabActivity.this, 0, i,
						PendingIntent.FLAG_ONE_SHOT);

				Notification notif = new NotificationCompat.Builder(TabActivity.this)
						.setSmallIcon(R.drawable.ic_launcher).setContentIntent(pIntent)
						.setContentTitle("Eye Clinic").setContentText("Lens Expired").build();

				notif.defaults = Notification.DEFAULT_ALL;
				notif.flags = Notification.FLAG_AUTO_CANCEL;

				NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				notifManager.notify(20, notif);
			}
		};

		cdTimer.start();
		SimpleDateFormat df = new SimpleDateFormat("D:M:yyyy:HH:mm");
		ApplicationHelper.preferences.edit().putString(scheduledCountDownDate, df.format(date)).commit();
	}

	OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View child, final int position, final long id) {
			CharSequence options[] = new CharSequence[] { "Delete" };

			AlertDialog.Builder builder = new AlertDialog.Builder(TabActivity.this);
			builder.setTitle("Please Select...");
			builder.setItems(options, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case 0:
						Log.e("iddd", "" + id);
						ApplicationHelper.database.deleteMedication(id);
						ApplicationHelper.medicationDataSet.remove(position);
						ApplicationHelper.medicationAdapter.notifyDataSetChanged();
						break;
					}
				}
			});
			builder.show();
			return true;
		}
	};

	public void onVideosButtonClick(View v) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);

		FragmentVideo fv = new FragmentVideo();
		ft.replace(R.id.tab5, fv, "fragment_video");
		ft.addToBackStack(null);
		ft.commit();
	}

	public void onGridButtonClick(View v) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);

		FragmentGrid fg = new FragmentGrid();
		ft.replace(R.id.tab5, fg, "fragment_grid");
		ft.addToBackStack(null);
		ft.commit();
	}

	public void onPrescriptionsButtonClick(View v) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left,
				R.anim.slide_out_to_right);

		FragmentPrescription fp = new FragmentPrescription(this);
		ft.replace(R.id.tab5, fp, "fragment_prescription");
		ft.addToBackStack(null);
		ft.commit();

		mi_addPrescriptionImage.setVisible(true);
	}
}
